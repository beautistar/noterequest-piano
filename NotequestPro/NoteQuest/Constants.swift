//
//  Constants.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 28.02.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation
import UIKit

//com.mentalstack.NoteQuestApp
//com.gracelee.notequest
struct AppSets{
    public static let PURCHASE = "com.gracelee.notequest.risingpaino"
    
    static let FB_URL = URL(string: "https://www.facebook.com/notequestapp")
    static let OFFICIAL_URL = URL(string: "http://notequest.net/")
    static let URL_CONTACTS = URL(string: "mailto:info@notequest.net")
    static let URL_APP_STORE = URL(string: "itms-apps://itunes.apple.com/us/app/note-quest-lite-learn-piano/id1176943393?mt=8")
    
    static let SUCCESS_SCORE_PERCENTS = 80
    static let START_DELAY = 2.0
    
    static let TEST_BUILD = true
}

struct Colors{
    static let navigationGradient = [#colorLiteral(red: 0.9579775929, green: 0.1760188341, blue: 0.4892035127, alpha: 1), #colorLiteral(red: 0.9683335423, green: 0.5949206352, blue: 0.2657420039, alpha: 1)]
    static let textBorder = #colorLiteral(red: 0.9579775929, green: 0.1760188341, blue: 0.4892035127, alpha: 1)
}

struct Storyboards{
    static let alerts = "Alerts"
    static let alerts_Ipad = "Alerts_Ipad"
    static let main = "Main"
    static let main_Ipad = "Main_Ipad"
    static let enterNameVC = "EnterNameVC"
    static let SessionVC = "SessionVC"
    static let SoundCheckVC = "SoundCheckVC"
    static let FinishLevelVC = "FinishLevelVC"
    static let SelectLevelVC = "SelectLevelVC"
    static let HelpVC = "HelpVC"
    static let LevelHelpVC = "LevelHelpVC"
    static let ScoreStatsVC = "ScoreStatsVC"
    
    static let customIpadElements = [
        SessionVC,
        SelectLevelVC,
        SoundCheckVC,
        enterNameVC,
        FinishLevelVC
    ]
}

struct SoundSets{
    static let FREQ_DISTANCE = 7
    static let FREQ_INTERVAL = 0.02
    static let MIN_AMPLITUDE = 0.1
    
    static let REAL_NOTE_DISTANCE = 0.012
    static let REAL_C = 261.63
}
