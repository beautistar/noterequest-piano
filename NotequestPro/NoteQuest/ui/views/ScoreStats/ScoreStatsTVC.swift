//
//  ScoreStatsTVC.swift
//  NoteQuest
//
//  Created by Yin on 2018/8/10.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import UIKit

class ScoreStatsTVC: MGSwipeTableCell {

    @IBOutlet weak var lblUser: UILabel!
    @IBOutlet weak var imvBoxBg: UIImageView!
    @IBOutlet weak var lblLevel: UILabel!
    @IBOutlet weak var lblTotalRounds: UILabel!
    @IBOutlet weak var lblTopPts: UILabel!
    @IBOutlet weak var imvStar: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
