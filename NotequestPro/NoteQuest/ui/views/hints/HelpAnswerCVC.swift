//
//  HelpAnswerCVC.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 09.03.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation
import UIKit

class HelpAnswerCVC:UICollectionViewCell{
    @IBOutlet var titleText:UILabel?
    @IBOutlet var descText:UILabel?
    
    private var targetUrl:URL?
    
    func initWithText(title:String, desc:String?, url:URL?){
        titleText?.text = title
        titleText?.adjustTextHeight()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(onTap))
        self.addGestureRecognizer(tap)
        
        if let url = url {
            descText?.attributedText = (desc ?? "").convertHtml()
            descText?.adjustTextHeight()
            targetUrl = url
            
        } else {
            descText?.text = desc ?? ""
            descText?.adjustTextHeight()
        }
    }
    
    @objc func onTap(){
        if let url = targetUrl {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
                
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
}
