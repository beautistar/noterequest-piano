//
//  HelpNewsCVC.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 09.03.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation
import UIKit

class HelpNewsCVC:UICollectionViewCell{
    @IBOutlet var titleText:UILabel?
    @IBOutlet var dot:UIImageView?
    
    func initWithText(text:String){
        titleText?.text = text
        titleText?.adjustTextHeight()
    }
}
