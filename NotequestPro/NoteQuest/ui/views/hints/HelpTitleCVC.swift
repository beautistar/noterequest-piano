//
//  HintTitleCVC.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 09.03.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation
import UIKit

class HelpTitleCVC: UICollectionViewCell{
    
    @IBOutlet var titleText: UILabel!
    
    func initWithText(text:String){
        titleText.text = text
 //       titleText.adjustTextHeight()
    }
}
