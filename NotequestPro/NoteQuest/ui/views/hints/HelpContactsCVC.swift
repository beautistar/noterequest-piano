//
//  HelpContactsCVC.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 09.03.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation
import UIKit

class HelpContactsCVC:UICollectionViewCell{
    @IBOutlet var titleText: UILabel?
    @IBOutlet var descText: UILabel?
    
    private var tap:UITapGestureRecognizer?
    
    @IBAction func onContactClick(_ sender: Any) {
        UIApplication.shared.open(AppSets.URL_CONTACTS!)
    }
    
    @IBAction func onFBClick(_ sender:Any){
        UIApplication.shared.open(AppSets.FB_URL!)
    }
    
    @objc func handleTap(_ sender:UITapGestureRecognizer){
        UIApplication.shared.open(AppSets.OFFICIAL_URL!)
    }

    func initWithText(title:String, desc:String?){
        if(tap == nil){
            tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
           
        }
        descText?.addGestureRecognizer(tap!)
        descText?.isUserInteractionEnabled = true
        titleText?.text = title
        titleText?.adjustTextHeight()
        
        if let desc = desc{
            descText?.attributedText = desc.convertHtml()
            descText?.adjustTextHeight()
        }
        
        
    }
}
