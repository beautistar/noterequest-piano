//
//  KeyBoardView.swift
//  PianoKeyBoard
//
//  Created by Ramesh on 21/09/16.
//  Copyright © 2016 Ramesh. All rights reserved.
//

import UIKit

protocol KeyboardViewDelegate {
    func keysPressed(frequency:Double)
}

let KEYBOARD_TAG_OFFSET = 100

class KeyBoardView: UIView {
    var delegate:KeyboardViewDelegate?
    var arrayOfBlackKeys: [Int] = [ -1, 4, -1, -1, 7, -1, 9, -1, -1, 12, -1, 14, -1, -1, 17, -1, 19, -1, -1, 1, -1, 1, -1]
    var keysArray: [BaseKey] = []
    private var starsArray:[UIImageView] = []
    
    var middleCPos = CGFloat(0)
    
    private var pianoSoundModule:PianoSoundModule?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
 
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func createKeyboard(height: CGFloat) {
        pianoSoundModule = PianoSoundModule()
        let whtieKeyWidth = height * 0.25
        let whtieKeyHeight = height
        let blackKeyWidth = height * 0.2
        let blackKeyHeight = height * 0.6
        var aXies = height * 0.1

     /*   if isIpad() {
             whtieKeyWidth = 56
             whtieKeyHeight = 285
             blackKeyWidth = 48
             blackKeyHeight = 195
            //titleLabelYaXis = 220
            aXies = 27
        }*/
        
        for i in 0..<29 {
            let keyWhite = WhiteKey(frame: CGRect(x: CGFloat(i)*whtieKeyWidth, y: 0, width: whtieKeyWidth, height: whtieKeyHeight))
            keyWhite.tag = KEYBOARD_TAG_OFFSET+i
            keyWhite.keyId = KEYBOARD_TAG_OFFSET+i
            keyWhite.image = UIImage(named: "ivory_keyup")
            keyWhite.highlightedImage = UIImage(named: "ivory_keyup")
            keyWhite.isHighlighted = false
            keyWhite.isUserInteractionEnabled = false
            
            if( i == 14) {
                let timati = UIImageView(frame: CGRect(
                    x: whtieKeyWidth/4,
                    y: whtieKeyHeight - whtieKeyWidth,
                    width: whtieKeyWidth/2,
                    height: whtieKeyWidth/2))
                timati.image = UIImage(named: "ic-star-black")
                timati.contentMode = UIViewContentMode.scaleAspectFit
                timati.isUserInteractionEnabled = false
                middleCPos = CGFloat(i)*whtieKeyWidth
                keyWhite.addSubview(timati)
            }
            
            let star = UIImageView(frame: CGRect(
                x: whtieKeyWidth/4,
                y: whtieKeyHeight - whtieKeyWidth,
                width: whtieKeyWidth/2,
                height: whtieKeyWidth/2))
            star.image = UIImage(named: "ic-star-red")
            star.contentMode = UIViewContentMode.scaleAspectFit
            star.isUserInteractionEnabled = false
            keyWhite.addSubview(star)
            star.tag = KEYBOARD_TAG_OFFSET+i
            self.addSubview(keyWhite)
            
            /*let lblTitle=UILabel()
            lblTitle.frame=CGRect(x: 0 ,y: titleLabelYaXis,width: whtieKeyWidth,height: 30)
            let strFreq = whiteKeysFreqArray[i]
            lblTitle.text = strFreq
            lblTitle.isUserInteractionEnabled = false
            lblTitle.textColor=UIColor.red
            lblTitle.font = UIFont(name: "Exo2-Regular", size: 12)
            lblTitle.textAlignment = NSTextAlignment.center
            keyWhite.addSubview(lblTitle)*/
            starsArray.append(star)
            keysArray.append(keyWhite)
        }
        
        for var i in 29..<49 {
            let keyBlack = BlackKey(frame: CGRect(x: aXies, y: 0, width: blackKeyWidth, height: blackKeyHeight))
            keyBlack.tag = KEYBOARD_TAG_OFFSET+i
            keyBlack.keyId = KEYBOARD_TAG_OFFSET+i
            keyBlack.image = UIImage(named: "ebony_keyup")
            keyBlack.highlightedImage = UIImage(named: "ebony_keyup")
            keyBlack.isHighlighted = false
            keyBlack.isUserInteractionEnabled = false
            self.addSubview(keyBlack)
            
            let star = UIImageView(frame: CGRect(
                x: blackKeyWidth/4,
                y: blackKeyHeight - blackKeyWidth,
                width: blackKeyWidth/2,
                height: blackKeyWidth/2))
            star.image = UIImage(named: "ic-star-red")
            star.contentMode = UIViewContentMode.scaleAspectFit
            star.isUserInteractionEnabled = false
            keyBlack.addSubview(star)
            star.tag = KEYBOARD_TAG_OFFSET+i
            
            starsArray.append(star)
            keysArray.append(keyBlack)
            i = i - 29
            if ( arrayOfBlackKeys[i]>0) {
                aXies = aXies+whtieKeyWidth
            }
            aXies = aXies+whtieKeyWidth
            self.bringSubview(toFront: keyBlack)
        }
        myWidth = Int(29*whtieKeyWidth)
        clearHint()
    }
    
    private var myWidth = 0
    func getMyWidth()->Int { return myWidth}
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        clearHint()
        self.touchesMoved(touches, with: event)
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        //if let touch = touches.first {
            //let currentPoint = touch.locationInView(self)
            //print(currentPoint)
        var pressedKeys = [BaseKey]()
            for keyIndex in 0 ..< keysArray.count {
                let key : BaseKey = keysArray[keyIndex];
                var keyIsPressed = false;
                for touch in touches {
                    let location = touch.location(in: self)
                     if(key.frame.contains(location)) {
                        var ignore = false
                        if (key .isKind(of: WhiteKey.self)) {
                            if (keyIndex > 0) {
                                let previousKey : BaseKey = keysArray[keyIndex-1];
                                if(previousKey.isKind(of: BlackKey.self) && previousKey.frame.contains(location)) {
                                    ignore = true;
                                }
                            }
                            
                            if (keyIndex < keysArray.count-1) {
                                let nextKey : BaseKey = keysArray[keyIndex+1];
                                if (nextKey .isKind(of: BlackKey.self)) {
                                    if(nextKey.frame.contains(location)) {
                                        ignore = true;
                                    }
                                }
                            }
                        }
                        
                        if (ignore == false) {
                            keyIsPressed = true;
                            if (key.isHighlighted == false){
                                key.isHighlighted = true;
                                if (delegate != nil) {
                                    pressedKeys.append(key)
                                }
                            }
                        }
                    }
                }
                
                if (keyIsPressed == false && key.isHighlighted == true) {
                    key.isHighlighted = false;
                }
            }
        //}
        
        guard let key : BaseKey = pressedKeys.popLast() else { return }
        guard let playedFrequency = pianoSoundModule?.playSound(tag: key.tag) else { return }
        delegate?.keysPressed(frequency: playedFrequency)
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let currentPoint = touch.location(in: self)
            for key in keysArray {
                if(key.frame.contains(currentPoint)) {
                    //print("Ra\(key)")
                    if (touch.phase == UITouchPhase.ended || touch.phase == UITouchPhase.cancelled) {
                        key.isHighlighted = false
                    } else {
                        print("Unhandled touch phase:\(touch.phase)")
                    }
                }
            }
        }
    }
    
    func hintFrequences(_ notes:[Double]?){
        guard let doubleNotes = notes else { return }
        var notes = [Int]()
        for note in doubleNotes{
            notes.append(Int(note))
        }
        guard let keyIds = pianoSoundModule?.getHintKeys(notes: notes) else { return }
        for star in starsArray{
            star.isHidden = !keyIds.contains(star.tag)
        }
    }
    
    func clearHint(){
        for star in starsArray{
            star.isHidden = true
        }
    }
    
    func dismiss(){
        pianoSoundModule?.finish()
    }
}
