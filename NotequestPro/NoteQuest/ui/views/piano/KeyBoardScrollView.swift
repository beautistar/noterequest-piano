//
//  KeyBoardScrollView.swift
//  PianoKeyBoard
//
//  Created by Ramesh on 21/09/16.
//  Copyright © 2016 Ramesh. All rights reserved.
//

import UIKit

class KeyBoardScrollView: UIScrollView {

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let currentPoint = touch.location(in: self)
            print(currentPoint)
            // do something with your currentPoint
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let currentPoint = touch.location(in: self)
            print(currentPoint)
            // do something with your currentPoint
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let currentPoint = touch.location(in: self)
            print(currentPoint)
            // do something with your currentPoint
        }
    }
}

extension KeyboardViewDelegate{

    func attachKeyboard( _ keyBoardScrollView:UIScrollView, height:CGFloat, width:CGFloat)->KeyBoardView {
        let keyBoard = KeyBoardView()
            let keyBoardHeight = height - 30
        keyBoard.createKeyboard(height:keyBoardHeight)
        let blackViewWidth = CGFloat(keyBoard.getMyWidth())
        
        keyBoardScrollView.contentSize = CGSize(width: blackViewWidth, height: keyBoardHeight)
        
        let blackView=UIView(frame: CGRect(x: 0, y: 0, width: blackViewWidth, height: 30))
        blackView.backgroundColor=UIColor.black
        keyBoardScrollView.addSubview(blackView)
        
        let border=UIView(frame: CGRect(x: 0, y: 28, width: blackViewWidth, height: 2))
        border.backgroundColor = Colors.textBorder
        keyBoardScrollView.addSubview(border)
        
        let lblTitle=UILabel()
        lblTitle.frame=CGRect(x: keyBoard.middleCPos ,y: -3,width: 180,height: 30)
        lblTitle.text = "note quest"
        lblTitle.isUserInteractionEnabled = false
        lblTitle.textColor=UIColor.white
        lblTitle.font = UIFont(name: "Ubuntu-Bold", size: CGFloat(27))
        lblTitle.textAlignment = NSTextAlignment.center
        blackView.addSubview(lblTitle)
        
        keyBoard.delegate = self
        keyBoard.frame = CGRect(x: 0, y: 30, width: blackViewWidth, height: keyBoardHeight)
        keyBoardScrollView.addSubview(keyBoard)
        let bottomOffset = CGPoint(x: (blackViewWidth-width)/2, y: 0)
        keyBoardScrollView.setContentOffset(bottomOffset, animated: true)
        return keyBoard
    }
}
