//
//  BlackKey.swift
//  PianoKeyBoard
//
//  Created by Ramesh on 20/09/16.
//  Copyright © 2016 Ramesh. All rights reserved.
//

import UIKit

class BlackKey: BaseKey {

    //var keyId = Int()
    override init(frame: CGRect) {
        // set myValue before super.init is called
        //self.myValue = 0
        super.init(frame: frame)
        
        //self.image = UIImage(named: "ebony_keyup")
        //self.highlightedImage = UIImage(named: "ebony_keydown")
        /*
         UIImage* ivoryKeyBackground = [UIImage imageNamed:@"ivory_keyup2.png"];
         UIImage* ivoryKeyHighlightBackground = [UIImage imageNamed:@"ivory_keydown2.png"];
         
         UIImage* ebonyKeyBackground = [UIImage imageNamed:@"ebony_keyup2.png"];
         UIImage* ebonyKeyHighlightBackground = [UIImage imageNamed:@"ebony_keydown2.png"];
         */
        //setImage(UIImage(named: "ebony_keyup"), forState: UIControlState.Normal)
        //setImage(UIImage(named: "ebony_keydown"), forState: UIControlState.Selected)
        //setImage(UIImage(named: "ebony_keydown"), forState: UIControlState.Application)
        //setImage(UIImage(named: "ebony_keydown"), forState: UIControlState.Highlighted)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
