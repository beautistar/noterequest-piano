//
//  SelectLevelTVC.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 14.03.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation
import UIKit

class SelectLevelTVC:UITableViewCell{
    @IBOutlet var pos:UILabel?
    @IBOutlet var title:UILabel?
    @IBOutlet var desc:UILabel?
    @IBOutlet var infoBtn:UIButton?
    @IBOutlet var container:UIView?
    @IBOutlet var back: UIImageView?
    
    @IBOutlet var proIcon: UIImageView!
    private var _levelID = 0
    private var parent:UIViewController?
    
    @IBAction func onInfoClick(_ sender: Any) {
       LevelHelp_hintLevelID = _levelID
       parent?.popup(Storyboards.LevelHelpVC, storyboard: Storyboards.alerts)
    }
    
    func show(_ item:LevelListDescription, parent:UIViewController){
        if let back = back{
             addShadowAndCorners(back:back)
        }
       infoBtn?.isHidden = item.levelHelp == nil
        infoBtn?.imageView?.contentMode = .scaleAspectFit
         proIcon.isHidden = !item.fromPay || PaymentService.isPayed(AppSets.PURCHASE)
        _levelID = item.num
        self.parent = parent
        title?.text = item.title
        desc?.text = item.desc
        pos?.text = item.leftTitle
    }
}
