//
//  SelectLevelExpandTVC.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 26.03.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation
import UIKit

class SelectLevelExpandTVC:UITableViewCell{
    @IBOutlet var back: UIImageView?
    @IBOutlet var titleText:UILabel?
    @IBOutlet var arrow:UIImageView?
    @IBOutlet var askBtn: UIButton?
    @IBOutlet var proIcon: UIImageView!
    
    var expanded = false
    var animated = false
    private  var _group:LevelGroups?
    private var parent:UIViewController?
    
    @IBAction func onAskClick(_ sender: Any) {
        LevelHelp_hintGroupID = _group
        parent?.popup(Storyboards.LevelHelpVC, storyboard: Storyboards.alerts)
    }
    
    func show(_ group:LevelGroup, parent:UIViewController, expanded:Bool){
        titleText?.text = group.title
        self.expanded = expanded
        if let arrow = self.arrow{
            if(expanded){
                arrow.transform = CGAffineTransform(rotationAngle: .pi)
            } else {
                arrow.transform = CGAffineTransform(rotationAngle:0)
            }
        }
        proIcon.isHidden =  !group.fromPay || PaymentService.isPayed(AppSets.PURCHASE)
        self.parent = parent
        askBtn?.isHidden = group.hint == nil
        _group = group.type
    }
    
    private var _prevGradient:CALayer?
 
    override var frame: CGRect{
        didSet{
            if let header = back{
                _prevGradient = header.setGradientBackground( Colors.navigationGradient, oldLayer: _prevGradient )
                addShadowAndCorners(back: header)
            }
        }
    }
    
    
    func toggle(){
        if animated { return }
        expanded = !expanded
        animated = true
        UIView.animate(withDuration: 0.5, animations: {
            if let arrow = self.arrow{
                arrow.transform = arrow.transform.rotated(by: .pi)
            }
        }, completion:{ _ in
            self.animated = false
        })
    }
}
