//
//  File.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 26.03.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation
import UIKit

class SelectLevelExpandedTVC:UITableViewCell{
    @IBOutlet var back:UIImageView?
    @IBOutlet var titleText:UILabel?
    @IBOutlet var descText:UILabel?
    @IBOutlet var askBtn: UIButton?
    
    @IBOutlet var proIcon: UIImageView!
    private var _levelID = 0
    private var parent:UIViewController?
    
    @IBAction func onInfoClick(_ sender: Any) {
        LevelHelp_hintLevelID = _levelID
        parent?.popup(Storyboards.LevelHelpVC, storyboard: Storyboards.alerts)
    }
    
    func show(_ item:LevelListDescription, parent:UIViewController){
        if let back = back{
            addShadowAndCorners(back:back)
        }
        askBtn?.isHidden = item.levelHelp == nil
        
        proIcon.isHidden = !item.fromPay || PaymentService.isPayed(AppSets.PURCHASE)
        _levelID = item.num
        self.parent = parent
        titleText?.text = item.title
        descText?.text = item.desc
        titleText?.textColor = item.titleColor ?? Colors.textBorder
    }
}
