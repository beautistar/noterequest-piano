//
//  BackgroundSelectView.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 27.02.2018.
//  Copyright © 2018 Ramesh. All rights reserved.
//

import UIKit

protocol BackgroundChangeListener:class{
    func onBackgroundChanged( _ image:BackgroundImage)
}

class BackgroundSelectView: UIView {
    fileprivate let cell = "cell"
    @IBOutlet var collection: UICollectionView!
    
    override init (frame : CGRect) {
        super.init(frame : frame)
        loadViewFromNib(with:BackgroundSelectView.self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib(with:BackgroundSelectView.self)
    }
    
   override func awakeFromNib() {
     collection.register(UINib(nibName:"BackgroundCVC", bundle: nil), forCellWithReuseIdentifier: cell)
     collection.dataSource = self
     collection.delegate = self
    }
    
    fileprivate var listeners:[BackgroundChangeListener] = []
    
    func addChangeListener(_ listener: BackgroundChangeListener){
        if let index = listeners.index(where: {$0 === listener}){
            listeners.remove(at: index)
        }
        listeners.append(listener)
    }
    
    func removeChangeListener(_ listener:BackgroundChangeListener){
        if let index = listeners.index(where: {$0 === listener}){
            listeners.remove(at: index)
        }
    }
}

extension BackgroundSelectView:UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return backgrounds.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let icon = collectionView.dequeueReusableCell(withReuseIdentifier: cell, for: indexPath) as! BackgroundCVC
        icon.show(backgrounds[indexPath.row])
        return icon
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let newImage = backgrounds[indexPath.row]
        setBackground(newImage)
        
        for listener in listeners {
            listener.onBackgroundChanged(newImage)
        }
    }
    
    
}

extension BackgroundSelectView:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if(isIpad()){
            return CGSize(width:143,height:193)
        } else {
            return CGSize(width:100, height:153)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if(isIpad()){
            let totalCellWidth = 143 * collectionView.numberOfItems(inSection: 0)
            let totalSpacingWidth = 10 * (collectionView.numberOfItems(inSection: 0) - 1)
            
            let leftInset = (collectionView.layer.frame.size.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
            let rightInset = leftInset
            
            return UIEdgeInsetsMake(0, leftInset, 0, rightInset)
        } else {
            return UIEdgeInsets(top: 0, left:30, bottom: 0, right: 30)
        }
    }
}

