//
//  BackgroundViewCell.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 27.02.2018.
//  Copyright © 2018 Ramesh. All rights reserved.
//

import Foundation
import UIKit

class BackgroundCVC: UICollectionViewCell{
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet var label: UITextView!
    @IBOutlet weak var iconCont:UIView?
    
    func show(_ data:BackgroundImage){
        if let image = getBackgroundIcon(data){
            icon.image = image
        }
        
        iconCont?.addShadowAndCorners(back:icon)
        label.text = data.name
    }
}
