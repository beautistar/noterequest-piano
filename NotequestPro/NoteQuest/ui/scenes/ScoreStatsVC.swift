//
//  ScoreStatsVC.swift
//  NoteQuest
//
//  Created by Yin on 2018/8/10.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import UIKit

class ScoreStatsVC: UIViewController, UITableViewDelegate, UITableViewDataSource, MGSwipeTableCellDelegate {
    
    

    @IBOutlet weak var back: UIImageView!
    @IBOutlet weak var tblScore: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initWindow(back, title: str.score)

        // Do any additional setup after loading the view.
        
        if let data = UserDefaults.standard.data(forKey: "score_stats"),
            let savedSoreStats = NSKeyedUnarchiver.unarchiveObject(with: data) as? [StatsModel] {
            scoreStats = savedSoreStats
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - TableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (isIpad()) {
            return 100
        } else {
            return 60
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return scoreStats.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ScoreStatsTVC") as! ScoreStatsTVC
        let statsObjc = scoreStats[indexPath.row]
        cell.lblUser.text = statsObjc.user_name
        cell.lblLevel.text = statsObjc.level
        cell.lblTotalRounds.text = "\(statsObjc.total_rounds)"
        cell.lblTopPts.text = "\(statsObjc.top_points)"
        switch statsObjc.stars {
        case 3:
            cell.imvStar.image = #imageLiteral(resourceName: "star_3")
            
        case 2: cell.imvStar.image = #imageLiteral(resourceName: "star_2")
        default:
            cell.imvStar.image = #imageLiteral(resourceName: "star_1")
        }
        
        //configure right buttons
        let padding = 15;
        let emailButton = MGSwipeButton(title: "", icon: #imageLiteral(resourceName: "ic-delete"), backgroundColor: .white, padding: padding, callback: { (cell) -> Bool in
            scoreStats.remove(at: indexPath.row)
            self.tblScore.reloadData()
            let userDefault = UserDefaults.standard
            let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: scoreStats)
            userDefault.set(encodedData, forKey: "score_stats")
            userDefault.synchronize()
            //cell.hideSwipe(animated: true)
            return true; //don't autohide to improve delete animation
        });
        
        let deleteButton = MGSwipeButton(title: "", icon: #imageLiteral(resourceName: "ic-email"), backgroundColor: .white, padding: padding, callback: { (cell) -> Bool in
            //cell.hideSwipe(animated: true)
            return true; //don't autohide to improve delete animation
        });
        
        cell.rightButtons = [emailButton, deleteButton]
        
        cell.rightSwipeSettings.transition = .static
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
//    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
//
//        let emailRowAction = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: "", handler:{action, indexpath in
//            print("MORE•ACTION");
//
//        });
//
//        emailRowAction.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "ic-email-60"));
//
//        let deleteRowAction = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: "", handler:{action, indexpath in
//            print("DELETE•ACTION");
//        });
//        deleteRowAction.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "ic-email-60"))
//        return [emailRowAction, deleteRowAction];
//
//    }
    

}
