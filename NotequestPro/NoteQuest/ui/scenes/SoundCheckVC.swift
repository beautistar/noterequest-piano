//
//  SoundCheckVC.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 01.03.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation
import UIKit

class SoundCheckVC:UIViewController{
    
    @IBOutlet var back: UIImageView!
    @IBOutlet var star: UIImageView!
    
    private var freqService:FrequencyService?
    private var starTimer:Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initWindow(back, title: str.sound_check, backTitle: str.back)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.freqService = FrequencyService(self)
        self.freqService?.start()
        
        starTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(SoundCheckVC.toggleStar), userInfo: nil, repeats: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        freqService?.stop()
        starTimer?.invalidate()
    }
    
    @objc private func toggleStar(){
      star.isHidden = !star.isHidden
    }
}

extension SoundCheckVC:IFrequenceListener{
    func listenFrequency(freq: Double) {
        if(checkRealNote(checked: freq, needed: SoundSets.REAL_C)){
            freqService?.stop()
            freqService = nil
            switchTo(Storyboards.SessionVC)
            print("oops")
        }
    }
}
