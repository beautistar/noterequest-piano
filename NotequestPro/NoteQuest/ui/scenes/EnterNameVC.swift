//
//  EnterNameVC.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 01.03.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation
import UIKit

class EnterNameVC: UIViewController{
    
    //@IBOutlet weak var header:UIView!
    @IBOutlet weak var inputText: UITextField!
    @IBOutlet weak var okButton:UIButton!
    @IBOutlet var mainCont: UIView!
    
    @IBAction func onCloseClick(_ sender: Any) {
       dismiss(animated: true)
    }
    
    @IBAction func onOkClick(_ sender: Any) {
        if let str = inputText.text{
            UserSets.name(value: str)
        }
        
        dismiss(animated: true)
    }
    
    private var _prevGradient:CALayer?
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        //_prevGradient = header.setGradientBackground( Colors.navigationGradient, oldLayer:_prevGradient )
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        inputText.text = UserSets.name(str.default_name)
        
        //_prevGradient = header.setGradientBackground( Colors.navigationGradient, oldLayer: _prevGradient )
        
        mainCont.layer.cornerRadius = 8
        mainCont.layer.masksToBounds = true
        
        inputText.delegate = self
        inputText.layer.cornerRadius = 4
        inputText.layer.borderWidth = 1
        inputText.layer.borderColor = Colors.textBorder.cgColor
        
      
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(UIViewController.onKeyboardShow),
                                               name: Notification.Name.UIKeyboardWillShow,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(UIViewController.onKeyboardHide),
                                               name: Notification.Name.UIKeyboardWillHide,
                                               object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        notifyWindow()
    }
}

extension UIViewController : UITextFieldDelegate {
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            if ((textField.text?.count)! + (string.count - range.length)) > 20 {
                return false
            }
        
        return true
    }

    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}

extension EnterNameVC:UINavigationElement{
    func navigationTag() -> String { return Storyboards.enterNameVC}
}
