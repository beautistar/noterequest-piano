//
//  FinishLevelVC.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 06.03.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation
import UIKit
import FBSDKShareKit

class FinishLevelVC: UIViewController {

    @IBAction func onCloseClick(_ sender: Any) {
        FinishSessionData.lastFinishedData?.state = FinishSessionResult.CANCEL
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onRepeatClick(_ sender: Any) {
        FinishSessionData.lastFinishedData?.state = FinishSessionResult.REPEAT
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onSelectLevelClick(_ sender: Any) {
        FinishSessionData.lastFinishedData?.state = FinishSessionResult.CHOOSE_LEVEL
        dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet var fbButton: UIButton!
    @IBOutlet var mainCont: UIView!
    @IBOutlet var timerLabel: UILabel!
    @IBOutlet var header: UIImageView!
    @IBOutlet var clockIcon: UIImageView!
    @IBOutlet var titleName: UILabel!
    @IBOutlet var desc: UILabel!
    
    var sound:SoundPlayer?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainCont.layer.cornerRadius = 8
        mainCont.layer.masksToBounds = true
        
        clockIcon.tintColor = UIColor.white
        
        var score = 0
        var stars = 0
        
        if let session = FinishSessionData.lastFinishedData {
            timerLabel.text = session.time.shortTextDesc()
            let level = LevelHelps.levelByID(UserSets.level())
          
            if(level?.groupID == .starter) {
                if(session.scores.stepsCompleted() >= 3 &&
                    session.scores.stepsCompleted() - session.scores.succCount() <= 1){
                     titleName.text = "Bravo, \(UserSets.name())!"
                    sound = SoundPlayer("applause")
                    sound?.play()
                    stars = 3
                } else {
                    titleName.text = "Let`s try again, \(UserSets.name())"
                    stars = 2
                }
                 desc.text = "You scored \(session.scores.succCount())/\(session.scores.stepsCompleted()) in \(level?.title ?? "")!"
                saveScoreStats(level: level!.title, score: session.scores.succCount(), stars: stars)
                
            } else {
                score = calcUserScore(data: session.scores, time: session.time, level: UserSets.level())
                if(score>=18){
                    titleName.text = "Bravo, \(UserSets.name())!"
                    sound = SoundPlayer("applause")
                    sound?.play()
                    stars = 3
                } else if score >= 16 {
                     titleName.text = "Try again, \(UserSets.name())"
                    stars = 2
                } else {
                     titleName.text = "Try reviewing Landmark Notes, \(UserSets.name())"
                    stars = 1
                }
                desc.text = "You scored \(score) points in Level \(level?.levelHelp?.title ?? level?.title ?? "")!"
                saveScoreStats(level: level?.levelHelp?.title ?? level?.title ?? "", score: score, stars: stars)
            }
            
        } else {
            
        }
    }
    
    func saveScoreStats(level: String, score: Int, stars: Int) {
        
        // retrieving a value for a key
        if let data = UserDefaults.standard.data(forKey: "score_stats"),
            let savedSoreStats = NSKeyedUnarchiver.unarchiveObject(with: data) as? [StatsModel] {
            
            scoreStats = savedSoreStats
            scoreStats.forEach({print( $0.user_name, $0.level, $0.top_points, $0.top_points, $0.stars)})
            
            for scorestat in scoreStats {
                if scorestat.level == level && scorestat.user_name == UserSets.name() {
                    scorestat.total_rounds += 1
                    if scorestat.top_points < score {
                        scorestat.top_points = score
                    }
                } else {
                    let statsObj = StatsModel(user_name: UserSets.name(), level: level, total_round: 1, top_points: score, stars: stars)
                    scoreStats.append(statsObj)
                }
            }            
        } else {
            
            let statsObj = StatsModel(user_name: UserSets.name(), level: level, total_round: 1, top_points: score, stars: stars)
            scoreStats.append(statsObj)
        }
        
        let userDefault = UserDefaults.standard
        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: scoreStats)
        userDefault.set(encodedData, forKey: "score_stats")
        userDefault.synchronize()
    }
    
    private var fbSharedBtn:FBSDKShareButton?
    private var _prevGradient:CALayer?
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(fbSharedBtn == nil){
            if let session = FinishSessionData.lastFinishedData{
                fbSharedBtn = shareToFB("Note Quest resing star score is \(session.scores.scoreStr()).", btnFB:fbButton, innerView:mainCont)
                fbButton.isHidden = true
            }
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        _prevGradient = header.setGradientBackground( Colors.navigationGradient, oldLayer:_prevGradient )
        fbSharedBtn?.frame = fbButton.frame
        fbSharedBtn?.roundCorners()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        notifyWindow()
        super.viewDidDisappear(animated)
    }
}

extension FinishLevelVC:UINavigationElement{
    func navigationTag() -> String {return Storyboards.FinishLevelVC}
}
