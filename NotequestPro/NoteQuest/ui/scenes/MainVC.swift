//
//  MainVC.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 28.02.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation
import UIKit
import SwiftyStoreKit

class MainVC: UIViewController{
    
    @IBOutlet var back: UIImageView!
    @IBOutlet var backgroundSelector: BackgroundSelectView?
    
    @IBOutlet var virtButton: UIButton!
    @IBOutlet var realButton: UIButton!
    @IBOutlet weak var midiButton: UIButton!
    
    var productId = "midi"
    
    @IBAction func onVirtualClick(_ sender: Any) {
       UserSets.mode(QuestMode.Virtual)
       screen(Storyboards.SelectLevelVC)
    }
    @IBAction func onRealClick(_ sender: Any) {
        UserSets.mode(QuestMode.Real)
        screen(Storyboards.SelectLevelVC)
    }
    
    @IBAction func onMidiClick(_ sender: Any) {
        
        UserSets.mode(QuestMode.Real)
        screen(Storyboards.SelectLevelVC)
        
        /*
            
         SwiftyStoreKit.purchaseProduct(productId, atomically: true, applicationUsername: "NoteQuest", completion: { result in
            switch result {
                
                case .success:
                    /*
                    self.UserSets.mode(QuestMode.Real)
                    self.screen(Storyboards.SelectLevelVC)
                    */
                break
         
                case .error(let error):
                    print(error)
                    /*
                    self._isPurchasing = false
                    self.hideHUD()
                    self.showAlertDialog(title: R.string.APP_TITLE, message: error.localizedDescription, positive: R.string.OK, negative: nil)
                    */
                break
            }
         }) 
         */
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initWindow(back, title: str.level_desc.replace(from:"%s", to: String(UserSets.level())))
      
        virtButton.isExclusiveTouch = true
        virtButton.imageView?.contentMode = .scaleAspectFit
        realButton.isExclusiveTouch = true
        realButton.imageView?.contentMode = .scaleAspectFit
        backgroundSelector?.isExclusiveTouch = true

        navigationController?.navigationBar.barStyle = UIBarStyle.black
        navigationController?.navigationBar.tintColor = UIColor.white
    //    navigationItem.leftBarButtonItems = buttons(image:"ic-levels", text:str.levels, target:self, selector: #selector(onLevelClick))
        navigationItem.rightBarButtonItems = buttons(image: "ic-info", text: str.info, target: self, selector: #selector(onHelpClick))
    
        navigationItem.backBarButtonItem = UIBarButtonItem(title: str.back, style: .done, target: nil, action: nil)
/*
        let noteView = NoteView(frame: CGRect(x:50,y:240, width:300, height:300))
        view.addSubview(noteView)
        noteView.drawNotes([
            NoteData(note: .C, octave:4, length: .L2, special: .flat),
            NoteData(note: .C, octave:2, length: .L4, special: .sharp, bottom:true),
            NoteData(note: .C, octave:4, length: .L4, special: .natural, bottom: true),
            NoteData.empty(length: .L2),
            NoteData(note: .C, octave:6, length: .L4, special: .natural)
            ])
      */
        popup(Storyboards.enterNameVC, storyboard: Storyboards.alerts)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = str.app_name
        backgroundSelector?.addChangeListener(self)
        super.viewWillAppear(animated)
    }
    
    @objc func onLevelClick(){
        screen(Storyboards.SelectLevelVC)
    }
    
    @objc func onHelpClick(){
        screen(Storyboards.HelpVC)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        backgroundSelector?.removeChangeListener(self)
        super.viewWillDisappear(animated)
    }
}

extension MainVC:BackgroundChangeListener{
    func onBackgroundChanged(_ image: BackgroundImage) {
        back.setBack(getBackground(image))
    }
}
