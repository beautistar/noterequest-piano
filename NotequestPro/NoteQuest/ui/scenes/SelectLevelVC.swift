//
//  SelectLevelVC.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 07.03.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD
import StoreKit

enum SelectLevelCellType{
    case level
    case subLevel
    case group
}

extension SelectLevelCellType{
    func str()->String{
        if (isIpad()){
            switch self {
            case .level: return "SelectLevelTVC_ipad"
            case .subLevel: return "SelectLevelExpandedTVC_ipad"
            case .group: return "SelectLevelExpandTVC_ipad"
            }
        } else {
            switch self {
            case .level: return "SelectLevelTVC"
            case .subLevel: return "SelectLevelExpandedTVC"
            case .group: return "SelectLevelExpandTVC"
            }
        }
    }
}

class SelectLevelVC:UIViewController{
    @IBOutlet var back:UIImageView?
    @IBOutlet var table: UITableView?
    private var tappedLevel:LevelListDescription?
    
    private var service:PaymentService?
    private var levels = [LevelListCell]()
    private var levelInfos = LevelHelps.constructLevelsList()
    private var expandedViews = [LevelListCell]()

    override func viewDidLoad() {
        super.viewDidLoad()
        initWindow(back, title: str.levels)
        
        navigationController?.navigationBar.barStyle = UIBarStyle.black
        navigationController?.navigationBar.tintColor = UIColor.white

//        let rightbutton = UIBarButtonItem.init(image: UIImage.init(named: "ic-trophy"), style: .plain, target: self, action: #selector(onScoreClick(_:)))
//        navigationItem.rightBarButtonItem = rightbutton
        navigationItem.rightBarButtonItems = buttons(image: "", text: str.score, target: self, selector: #selector(onScoreClick))
        //navigationItem.leftBarButtonItems = buttons(image:"ic-levels", text:str.levels, target:self, selector: #selector(onScoreClick))
        //navigationItem.title = str.levels
        navigationItem.backBarButtonItem = UIBarButtonItem(title: str.back, style: .done, target: nil, action: nil)
        
        levelInfos.forEach{levels.append($0)}
        
        table?.register(UINib(nibName:SelectLevelCellType.group.str(), bundle: nil), forCellReuseIdentifier: SelectLevelCellType.group.str())
        table?.register(UINib(nibName:SelectLevelCellType.level.str(), bundle: nil), forCellReuseIdentifier: SelectLevelCellType.level.str())
        table?.register(UINib(nibName:SelectLevelCellType.subLevel.str(), bundle: nil), forCellReuseIdentifier: SelectLevelCellType.subLevel.str())
        table?.dataSource = self
        table?.delegate = self
        if(isIpad()){
            table?.contentInset = UIEdgeInsets(top: 15, left: 0, bottom: 15, right: 0)
        } else {
            table?.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        }
        service = PaymentService(self, payments: [AppSets.PURCHASE])
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        notifyWindow()
        service?.dismiss()
    }
    
    @objc func onScoreClick(_ sender: UIButton) {
        switchTo(Storyboards.ScoreStatsVC)        
    }
    
    private func processLevel(data:LevelListDescription){
        tappedLevel = data
        
        if (data.num == 0){
            alert(str.coming_soon)
            return
        }
    
        if !AppSets.TEST_BUILD && ( data.fromPay && !PaymentService.isPayed(AppSets.PURCHASE)){
            if(InternetReachability.isConnectedToNetwork()){
                if service?.pay(AppSets.PURCHASE) != true {
                    onEmptyList()
                }
            } else {
                alert(str.bad_internet)
            }
        } else {
            if(UserSets.mode() == .Real){
                UserSets.level(data.num)
           //     switchTo(Storyboards.SessionVC)
                switchTo(Storyboards.SoundCheckVC)
            } else {
                if(data.canVirtual){
                    UserSets.level(data.num)
                    switchTo(Storyboards.SessionVC)
                } else {
                    alert(str.virtual_mode_lock_desc)
                }
            }
        }
    }
}

extension SelectLevelVC:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return levels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var icon:UITableViewCell!
        let data = levels[indexPath.row]
        switch data.type {
        case .level:
            icon = tableView.dequeueReusableCell(withIdentifier: SelectLevelCellType.level.str(), for:indexPath)
            (icon as? SelectLevelTVC)?.show(data.level!, parent: self)
            break
        case .group:
            icon = tableView.dequeueReusableCell(withIdentifier: SelectLevelCellType.group.str(), for:indexPath)
            (icon as? SelectLevelExpandTVC)?.show(data.group!, parent:self, expanded: expandedViews.index(of: data) != nil)
            break
        case .sublevel:
             icon = tableView.dequeueReusableCell(withIdentifier: SelectLevelCellType.subLevel.str(), for:indexPath)
             (icon as? SelectLevelExpandedTVC)?.show(data.level!, parent:self)
        }
    
        icon.selectionStyle = UITableViewCellSelectionStyle.none;
        icon.isExclusiveTouch = true
        return icon
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = levels[indexPath.row]
        
        if let level = data.level{
            processLevel(data: level)
        } else if let group = data.group {
            if !group.released{
                alert(str.coming_soon)
                return
            }
            
            if let cell = tableView.cellForRow(at: indexPath) as? SelectLevelExpandTVC{
                if cell.animated { return }
                
                cell.toggle()
                if(!cell.expanded){
                    if let idx = expandedViews.index(of: data)?.value(){
                           expandedViews.remove(at: Int(idx) )
                    }
                 
                    var childIndexes = [Int]()
                    var renovedNums = 0
                    group.subLevels.forEach{ removedLevel in
                        let index = Int(levels.index{ $0 == removedLevel }?.value() ?? -1)
                        if index >= 0 {
                            childIndexes.append(index + renovedNums)
                            levels.remove(at: index)
                            renovedNums += 1
                        }
                    }
                    tableView.removeChildCells(for:indexPath, removeChildsIndexes: childIndexes, dataSource: levels)
                } else {
                    expandedViews.append(data)
                    group.subLevels.forEach({ (subItem) in
                        let subItemIdx = group.subLevels.index(of: subItem)
                        levels.insert(subItem, at: indexPath.row + subItemIdx! + 1)
                    })
                    let subItemsIndexPaths:[IndexPath] = group.subLevels.map({ (subItem) -> IndexPath in
                        let itemIdx = group.subLevels.index(of: subItem)
                        return IndexPath.init(row: indexPath.row + itemIdx! + 1, section: indexPath.section)
                    })
                    tableView.beginUpdates()
                    tableView.insertRows(at: subItemsIndexPaths, with: .none)
                    tableView.endUpdates()
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let data = levels[indexPath.row]
        if(isIpad()){
            switch data.type {
            case .sublevel: return 108
            case .group: return 132
            case .level: return 132
            }
        } else {
            switch data.type {
            case .sublevel: return 66
            case .group: return 78
            case .level: return 80
        }
        }
    }
}

extension SelectLevelVC:UINavigationElement{
    func navigationTag() -> String { return Storyboards.SelectLevelVC }
}

extension SelectLevelVC:IPaymentListener{
    func requestBundle(bundle:SKProduct, onSuccess: @escaping (Bool)->Void) {
        if(InternetReachability.isConnectedToNetwork()){
  
            let alert = UIAlertController(title: bundle.localizedTitle, message: str.payment_desc, preferredStyle: UIAlertControllerStyle.alert )
            let purchseAction = UIAlertAction(title: str.yes, style: UIAlertActionStyle.default, handler: {Void in
                onSuccess(true)
            })
            let action = UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: {Void in onSuccess(false)})
            alert.addAction(purchseAction)
            alert.addAction(action)
            navigationController!.present(alert, animated: true, completion:nil)
        } else {
            alert(str.bad_internet)
        }
    }
    
    func paymentListener(bundle: String?) {
        if bundle != nil, let level = self.tappedLevel{
            self.processLevel(data: level)
        }
    }
    
    func loadingBlock(_ loaded: Bool) {
        if(loaded){
            MBProgressHUD.showAdded(to: self.view, animated: true)
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    
    func onEmptyList() { alert(str.payments_mot_loaded) }
}


extension UITableView{
    func removeChildCells(for objectIndexPath:IndexPath, removeChildsIndexes:[Int],dataSource:Any) {
        let indexesForRemoving = generateChildsIndexPaths(for: objectIndexPath, childsIndexes: removeChildsIndexes)
        beginUpdates()
        self.deleteRows(at: indexesForRemoving, with: .none)
        endUpdates()
    }
    
    private func generateChildsIndexPaths(for rootIndexPath:IndexPath,childsIndexes:[Int]) -> [IndexPath]{
        return childsIndexes.map { (indx) -> IndexPath in
            return IndexPath.init(row: indx, section: rootIndexPath.section)
        }
    }
}
