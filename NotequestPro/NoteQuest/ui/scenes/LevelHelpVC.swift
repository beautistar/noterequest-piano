//
//  LevelHelpVC.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 13.03.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation
import UIKit
var LevelHelp_hintLevelID:Int = 1
var LevelHelp_hintGroupID:LevelGroups?

class LevelHelpVC: UIViewController{
    
    @IBOutlet var mainCont: UIView!
    @IBOutlet var descCont: UIView!
    @IBOutlet var img: UIImageView!
    @IBOutlet var descText: UILabel!
    @IBOutlet var levelText: UILabel!
    @IBOutlet var back: UIImageView!
    @IBOutlet var titleText: UILabel!
    @IBAction func onCloseClick(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainCont.layer.cornerRadius = 8
        mainCont.layer.masksToBounds = true
        
        var hint:LevelHelp?
        if let groupType = LevelHelp_hintGroupID{
            hint = LevelHelps.getGroupByType(groupType).hint
        }
        
        if let data = hint ?? LevelHelps.levelByID(LevelHelp_hintLevelID)?.levelHelp ?? nil{
            levelText.text = data.title
            descText.text = data.desc
            
            if let image = data.image {
                img.image = image
            } else {
                img.heightAnchor.constraint(equalTo: img.widthAnchor, multiplier: 1/200).isActive = true
            }
        }
        
        LevelHelp_hintGroupID = nil
        LevelHelp_hintLevelID = -1
    }
    
    private var _prevGradient:CALayer?
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        _prevGradient = back.setGradientBackground( Colors.navigationGradient, oldLayer:_prevGradient )
    }
    
    
}
