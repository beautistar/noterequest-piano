//
//  VirtualModeVC.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 01.03.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation
import UIKit

class SessionVC:UIViewController{
    
    @IBAction func helpClick(_ sender: UIButton) {
        logic?.presentHint()
    }
    
    @IBOutlet var noteImageView: UIImageView!
    @IBOutlet var hintText: UILabel!
    @IBOutlet var noteCont: UIView!
    @IBOutlet var hintBtn: UIButton!
    @IBOutlet var keyboardScroll:UIScrollView!
    @IBOutlet var keyboard: UIView!
    @IBOutlet var back: UIImageView!
    @IBOutlet var timerText: UILabel!
    @IBOutlet var timerIcon: UIImageView!
    @IBOutlet var HintCommonText: UILabel!
    @IBOutlet var nextBtn:UIButton?
    @IBOutlet var backBtn:UIButton?
    @IBOutlet var noteView:NoteView?
    private var logic:SessionService!
    
    @IBAction func onBackClick(_ sender: Any) {
        guard let scrollView = keyboardScroll else { return }
        let bottomOffset = CGPoint(x: 0, y: 0)
        scrollView.setContentOffset(bottomOffset, animated: true)
    }
    
    @IBAction func onNextClick(_ sender: Any) {
        guard let scrollView = keyboardScroll else { return }
        let bottomOffset = CGPoint(x: scrollView.contentSize.width - scrollView.bounds.size.width, y: 0)
        scrollView.setContentOffset(bottomOffset, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        if let desc = LevelHelps.levelByID(UserSets.level()){
            let title = desc.levelHelp?.title ?? desc.title
              initWindow(back, title: title, backTitle: str.back)
        } else {
              initWindow(back, title: "", backTitle: str.back)
        }
       
        hintBtn.isExclusiveTouch = true
        
        let btnItem = UIBarButtonItem(title: str.skip, style: .done, target:self, action:#selector(onSkip))
        btnItem.customView?.isExclusiveTouch = true
        navigationItem.rightBarButtonItem = btnItem
        
        logic = SessionService(self)
        logic.startSession()
        
        if(isIpad()){
            hintBtn.imageView?.contentMode = .scaleAspectFit
        }
    }
    
    @objc func onSkip(){
        logic.skip()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        logic?.disappear()
    }
    
    func presentTime(_ str:String){
        DispatchQueue.main.async {
            self.timerText.text = str
        }
    }
    
    @objc func clearHint(){
       showHint("")
    }
    
    func showHint(_ str:String){
        DispatchQueue.main.async {
            self.hintText.attributedText = str.formatToNote()
            self.HintCommonText.isHidden = str == ""
        }
    }
    
    func presentNode(mode:QuestMode){
        backBtn?.isHidden = mode != QuestMode.Virtual
        nextBtn?.isHidden = mode != QuestMode.Virtual
        keyboardScroll.isHidden = mode != QuestMode.Virtual
        if (mode == .Virtual){
            keyboardScroll.delegate = self
        }
        
        hintText.isHidden = mode != QuestMode.Real
    }
    
    func presentImage( _ note:NoteCell){
        self.noteView?.drawNotes( note.frequences )
    }
}

extension SessionVC:UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        updateButtons(scrollView)
    }
    
    private func updateButtons (_ scrollView:UIScrollView){
        backBtn?.isHidden = scrollView.contentOffset.x <= 0
        nextBtn?.isHidden = scrollView.contentOffset.x >= scrollView.contentSize.width - scrollView.bounds.size.width
    }
}
