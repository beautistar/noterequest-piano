//
//  File.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 09.03.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation
import UIKit

class HelpVC:UIViewController{
    @IBOutlet var back:UIImageView?
    @IBOutlet var list:UICollectionView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initWindow(nil, title: str.info)
        
        list?.register(UINib(nibName:"HelpTitleCVC", bundle: nil), forCellWithReuseIdentifier: getcellID(.header))
        list?.register(UINib(nibName:"HelpNewsCVC", bundle: nil), forCellWithReuseIdentifier: getcellID(.news))
        list?.register(UINib(nibName:"HelpAnswerCVC", bundle: nil), forCellWithReuseIdentifier: getcellID(.answers))
        list?.register(UINib(nibName:"HelpContactsCVC", bundle: nil), forCellWithReuseIdentifier: getcellID(.contacts))
        
        let flowLayout = LeftAlignedCollectionViewFlowLayout()
        flowLayout.addOffset = (UIScreen.main.bounds.width - (list?.frame.size.width ?? 0)) / 2
        flowLayout.estimatedItemSize = CGSize(width: UIScreen.main.bounds.width, height:1)
        flowLayout.itemSize = CGSize(width: UIScreen.main.bounds.width, height:1)
        
        flowLayout.minimumInteritemSpacing = 1000
        list?.collectionViewLayout = flowLayout

        list?.translatesAutoresizingMaskIntoConstraints = false
        list?.dataSource = self
        list?.delegate = self
    }
}

extension HelpVC:UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return helpDesc.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let data = helpDesc[indexPath.row]
        let icon = collectionView.dequeueReusableCell(withReuseIdentifier: getcellID(data.type), for: indexPath)
    
        switch data.type {
        case .header:
            (icon as? HelpTitleCVC)?.initWithText(text: data.title)
            break
            
        case .news:
            (icon as? HelpNewsCVC)?.initWithText(text:data.title)
            break
            
        case .answers:
            (icon as? HelpAnswerCVC)?.initWithText(title:data.title, desc:data.desc, url:data.url)
            break
            
        case .contacts:
            (icon as? HelpContactsCVC)?.initWithText(title:data.title, desc: data.desc)
            break
        }
     
        return icon
    }

}
private func getcellID(_ type: HelpCellType)->String{
    switch type {
    case .header: return "header"
    case .news: return "news"
    case .answers: return "answers"
    case .contacts: return "contacts"
    }
}
private let helpDesc = [
  HelpCell(type:.header, title: "What’s new in Note Quest?", desc: nil, url:nil),
   HelpCell(type:.news, title: "New! “Hint” button - for those who are new to reading notes on piano, it will reveal the correct notes. Makes for an interactive learning experience, not just drilling “right or wrong” notes or pitches. ", desc: nil, url:nil),
    HelpCell(type:.news, title: "New! Along with a complete redesign, choose from five fun backgrounds to increase your learning while going places!", desc: nil, url:nil),
    HelpCell(type:.header, title: "Frequently Asked Questions", desc: nil, url:nil),
    HelpCell(type:.answers, title:"I’m in Real Piano, but why doesn’t it hear me?", desc: "Go to your device’s Settings, and make sure you have enabled your iPad/iPhone’s microphone. Also, make sure your volume is set appropriately.", url:nil),
    HelpCell(type:.answers, title: "How is the score calculated?", desc: "Your accuracy is calculated, then сonverted into points, and bonus points are added for speed. Tapping the Hint or Skip button will decrease your score by 1 point.", url:nil),
    HelpCell(type:.answers, title: "Why can’t I play Level 4 or 5 in Virtual Piano? ", desc: "Levels 4 and 5 require a wider keyboard range than is practical for most devices. We believe that practicing on a real piano gives you the best experience learning to read.", url:nil),
    HelpCell(type:.answers, title: "How can I support Note Quest?", desc: "<font face=\"Ubuntu\" size=\"4\">Note Quest started with a piano teacher’s dream and made from scratch! So every time a great review is posted, we do a happy dance! Your suggestions and praises make a difference. Leave a <b><u>review</u></b> and know that you did a good deed today.</font>", url: AppSets.URL_APP_STORE ),
    HelpCell(type:.header, title: "Contact Us", desc:nil, url:nil),
   HelpCell(type:.contacts, title: "If you have any suggestions or comments, please contact us.", desc: "<font face=\"Ubuntu\" size=\"4\">Visit <b><u>www.notequest.net</u></b> to stay on top of updates and enter fun contests by joining our email list.</font>", url:nil),
    HelpCell(type:.header, title:"", desc:nil, url:nil)
]

private enum HelpCellType{
    case header
    case news
    case answers
    case contacts
}

private struct HelpCell{
    let type:HelpCellType
    let title:String
    let desc:String?
    let url:URL?
}

class LeftAlignedCollectionViewFlowLayout: UICollectionViewFlowLayout {
    var addOffset:CGFloat = 0
    
 /*   override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        let attributes = super.layoutAttributesForElements(in: rect)
        
        var leftMargin = sectionInset.left
        var maxY: CGFloat = -1.0
        attributes?.forEach { layoutAttribute in
            if layoutAttribute.frame.origin.y >= maxY {
                leftMargin = sectionInset.left
            }
            
            layoutAttribute.frame.origin.x = leftMargin + addOffset
            
            leftMargin += layoutAttribute.frame.width + minimumInteritemSpacing
            maxY = max(layoutAttribute.frame.maxY , maxY)
        }
        
        return attributes
    }*/
}
