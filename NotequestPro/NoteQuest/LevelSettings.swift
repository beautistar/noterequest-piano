//
//  LevelSettings.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 05.03.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation
import UIKit

struct LevelHelp {
    let title:String
    let desc:String
    let image:UIImage?
}

struct LevelListDescription{
    let num:Int
    let leftTitle:String
    let time:Int
    let title:String
    let desc:String
    let levelHelp:LevelHelp?
    let canVirtual:Bool
    let fromPay:Bool
    let groupID:LevelGroups?
    let titleColor:UIColor?
}

struct LevelGroup{
    let type: LevelGroups
    let title:String
    var subLevels: [LevelListCell]
    let hint:LevelHelp?
    let fromPay:Bool
    let released:Bool
}

enum LevelListCells{
    case group
    case level
    case sublevel
}
enum LevelGroups{
    case starter
    case intervals
}

class LevelListCell:NSObject{
    let type: LevelListCells
    let group: LevelGroup?
    let level:LevelListDescription?
    init(type:LevelListCells, group:LevelGroup?, level:LevelListDescription?) {
        self.type = type
        self.group = group
        self.level = level
    }
}

class LevelHelps{
    static func getGroupByType(_ type:LevelGroups)-> LevelGroup{
        switch type {
        case .starter: return
            LevelGroup(type: .starter,
                       title: "Landmark Notes",
                       subLevels: [],
                       hint: LevelHelp(title: "Landmark Notes", desc: "What’s a Landmark Note or Guide Note? It’s a selected note on the staff that helps you easily recognize other notes around it. ", image: nil),
                       fromPay: false,
                       released: true
            )
        case .intervals: return
            LevelGroup(type: .intervals,
                       title: "Interval Selector",
                       subLevels: [],
                       hint: nil,
                        fromPay: true,
                        released:true
            )
        }
    }
    
    private static let levels = [
        LevelListDescription(num: 1001,
                             leftTitle: "S",
                             time: 15,
                             title: "3 Landmark Notes",
                             desc: "Middle C, Bass F, Treble G",
                             levelHelp: LevelHelp(title: "3 Landmark Notes", desc: "Middle C, Bass F, Treble G", image: #imageLiteral(resourceName: "Landmark-Notes")),
                             canVirtual: true,
                             fromPay: false,
                             groupID: .starter,
                             titleColor: #colorLiteral(red: 0.9579775929, green: 0.1760188341, blue: 0.4892035127, alpha: 1)
        ),
        LevelListDescription(num: 1002,
                             leftTitle: "S",
                             time: 15,
                             title: "The 3 C’s",
                             desc: "Bass C, Middle C, Treble C",
                             levelHelp: LevelHelp(title: "The 3 C’s", desc: "Bass C, Middle C, Treble C", image: #imageLiteral(resourceName: "3C")),
                             canVirtual: true,
                             fromPay: false,
                             groupID: .starter,
                             titleColor: #colorLiteral(red: 0.2429464161, green: 0.6907173395, blue: 0.942668736, alpha: 1)
        ),
        LevelListDescription(num: 1003,
                             leftTitle: "S",
                             time: 15,
                             title: "The 5 C’s",
                             desc: "Low C to High C (ledger line C’s)",
                             levelHelp: LevelHelp(title: "The 5 C’s", desc: "Low C to High C (ledger line C’s)", image: #imageLiteral(resourceName: "5C") ),
                             canVirtual: true,
                             fromPay: false,
                             groupID: .starter,
                             titleColor: #colorLiteral(red: 0.309658736, green: 0.6714266539, blue: 0.02120847814, alpha: 1)
        ),
        LevelListDescription(num: 1004,
                             leftTitle: "S",
                             time: 30,
                             title: "Landmark Note Mix",
                             desc: "All Landmark Notes",
                             levelHelp: LevelHelp(title: "Landmark Note Mix", desc: "All landmark notes", image: #imageLiteral(resourceName: "note-mix")),
                             canVirtual: true,
                             fromPay: false,
                             groupID: .starter,
                             titleColor: #colorLiteral(red: 0.96184057, green: 0.4250659347, blue: 0.1783950627, alpha: 1)
        ),
        LevelListDescription(num:1,
                             leftTitle: "1",
                             time: 60,
                             title: "Starter",
                             desc: "Middle C & Friends",
                             levelHelp: LevelHelp(title: "Level 1", desc: "Bass F up to Treble G, 9 pitches only", image: #imageLiteral(resourceName: "level1")),
                             canVirtual: true,
                             fromPay: true,
                             groupID: nil,
                             titleColor: nil
        ),
        LevelListDescription(num:2,
                             leftTitle: "2",
                             time: 80,
                             title: "Rising Star",
                             desc: "Bass C to Treble C, 2 octaves",
                             levelHelp:  LevelHelp(title: "Level 2", desc: "Bass C up to Treble C, 2 octave range", image: #imageLiteral(resourceName: "level2")),
                             canVirtual: true,
                             fromPay: true,
                              groupID: nil,
                             titleColor: nil
        ),
        LevelListDescription(num:3,
                             leftTitle: "3",
                             time: 90,
                             title: "Two Stepper",
                             desc: "+ Intervals, 2 octave range",
                             levelHelp:  LevelHelp(title: "Level 3", desc: "Includes 2-note intervals, 2 octave range, white keys", image: #imageLiteral(resourceName: "level3")),
                             canVirtual: true,
                             fromPay: true,
                             groupID: nil,
                             titleColor: nil
        ),
        LevelListDescription(num:4,
                             leftTitle: "4",
                             time: 90,
                             title: "Navigator",
                             desc: "+ ♭ # ♮, 3 octave range",
                             levelHelp:  LevelHelp(title: "Level 4", desc: "Includes black keys (# ♭ ♮) 3 octave range, all keys ", image: #imageLiteral(resourceName: "level4")),
                             canVirtual: false,
                             fromPay: true,
                             groupID: nil,
                             titleColor: nil
        ),
        LevelListDescription(num:5,
                             leftTitle: "5",
                             time: 90,
                             title: "Master",
                             desc: "+ Ledger lines, 4 octave range",
                             levelHelp: LevelHelp(title: "Level 5", desc: "Includes ledger line notes. All of the previous concepts: 4 octave range, all keys & intervals", image: #imageLiteral(resourceName: "level5")),
                             canVirtual: false,
                             fromPay: true,
                            groupID: nil,
                             titleColor: nil
        ),
        LevelListDescription(num:1102,
                             leftTitle: "",
                             time: 80,
                             title: "2nds",
                             desc: "",
                             levelHelp: nil,
                             canVirtual: true,
                             fromPay: true,
                             groupID: .intervals,
                             titleColor: nil
        ),
        LevelListDescription(num:1103,
                             leftTitle: "",
                             time: 80,
                             title: "3rds",
                             desc: "",
                             levelHelp: nil,
                             canVirtual: true,
                             fromPay: true,
                             groupID: .intervals,
                             titleColor: nil
        ),
        LevelListDescription(num:0,
                             leftTitle: "",
                             time: 0,
                             title: "4ths",
                             desc: "",
                             levelHelp: nil,
                             canVirtual: false,
                             fromPay: true,
                              groupID: .intervals,
                             titleColor: nil
        ),
        LevelListDescription(num:0,
                             leftTitle: "",
                             time: 0,
                             title: "5ths",
                             desc: "",
                             levelHelp: nil,
                             canVirtual: false,
                             fromPay: true,
                             groupID: .intervals,
                             titleColor: nil
        ),
        LevelListDescription(num:0,
                             leftTitle: "",
                             time: 0,
                             title: "6ths",
                             desc: "",
                             levelHelp: nil,
                             canVirtual: false,
                             fromPay: true,
                             groupID: .intervals,
                             titleColor: nil
        ),
        LevelListDescription(num:0,
                             leftTitle: "",
                             time: 0,
                             title: "7ths & 8ths",
                             desc: "",
                             levelHelp: nil,
                             canVirtual: false,
                             fromPay: true,
                             groupID: .intervals,
                             titleColor: nil
        )
    ]
    
    static func level(_ pos:Int)->LevelListDescription?{
        if( pos >= 0 && pos < levels.count){
            return levels[pos]
        } else {
            return nil
        }
    }
    
    static func levelByID( _ id:Int)->LevelListDescription?{
        for sets in levels{
            if sets.num == id{
                return sets
            }
        }
        return nil
    }
    
    static func levelTime(levelID:Int)->Int{
        return levelByID(levelID)?.time ?? 5
    }

        static func constructLevelsList()-> [LevelListCell]{
            var result = [LevelListCell]()
            var prevGroup: LevelGroup?
            
            for level in levels{
                if let group = level.groupID{
                    if(prevGroup == nil){
                       prevGroup = getGroupByType(group)
                    }
                    prevGroup?.subLevels.append(LevelListCell(type: .sublevel, group:nil, level: level))
                } else {
                    if let prevGroup = prevGroup{
                        result.append(LevelListCell(type:.group, group:prevGroup, level:nil))
                    }
                    prevGroup = nil
                    result.append(LevelListCell(type: .level, group:nil, level: level))
                }
            }
            
            if let prevGroup = prevGroup{
                result.append(LevelListCell(type:.group, group:prevGroup, level:nil))
            }
            prevGroup = nil
            
            return result
        }
}
