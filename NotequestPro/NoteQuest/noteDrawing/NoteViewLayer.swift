//
//  NoteViewLayer.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 21.03.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation
import UIKit

class NoteViewLayer:CALayer{
    private var sets: NoteViewSets!
    private var notes: NoteBar?
    
    func initialize(frame: CGRect, sets:NoteViewSets){
        self.sets = sets;
        self.frame = frame
    }
    
    public func drawNotes(_ notes:NoteBar){
        self.notes = notes
        redraw()
    }
    
    private func w()->CGFloat{ return frame.width }
    private func h()->CGFloat{ return frame.height }
    
    private func redraw(){
        let branch = NoteViewSets.branch
        let branchSize = branch.calcScaledSize(fullSize: h(), scale: sets.botBlockCenter - sets.topBlockCenter + sets.lineDistance*4)
        var leftOffset:CGFloat = branchSize.width
        
        //brach
        
        draw( branch,
                       posX: branchSize.width*branch.dX,
                       posY: h()*(sets.topBlockCenter + (sets.botBlockCenter - sets.topBlockCenter)*0.5),
                       size: branchSize )
        
        leftOffset += w() * 0.02
        
        //lines
        
        backgroundColor = sets.backColor.cgColor
       
        fiveLineBlock(yPos: h() * sets.topBlockCenter, leftOffset: leftOffset)
        fiveLineBlock(yPos: h() * sets.botBlockCenter, leftOffset: leftOffset)
        
        drawLine(
            color: sets.lineColor.cgColor,
            sPos: CGPoint(x:leftOffset + h()*sets.lineDepth*0.5, y: h() * (sets.topBlockCenter - sets.lineDistance * 2) ),
            fPos: CGPoint(x:leftOffset + h()*sets.lineDepth*0.5, y: h() * (sets.botBlockCenter + sets.lineDistance * 2) ),
            depth: h() * sets.lineDepth)
        
        leftOffset += w() * 0.02
        
        //decors
        
        let bass = NoteViewSets.bassNote
        let bassSize = bass.calcScaledSize(fullSize: h())
        draw(bass,
                      posX: leftOffset + bassSize.width * bass.dX,
                      posY: h() * sets.botBlockCenter,
                      size: bassSize)
        
        let norm = NoteViewSets.normNote
        let normSize = norm.calcScaledSize(fullSize: h())
        draw(norm,
                      posX: leftOffset + normSize.width * norm.dX,
                      posY: h() * sets.topBlockCenter,
                      size: normSize)
        
        leftOffset = leftOffset + max(bassSize.width, normSize.width)
        
        //notes
        
        if let notes = notes {
            let step = (w() - leftOffset)/CGFloat(notes.count + 1)
            var noteDistance = leftOffset + step
            
            for note in notes{
                drawNote(note, xPos: noteDistance)
                noteDistance += step
            }
        }
    }
    
    private func fiveLineBlock(yPos:CGFloat, leftOffset:CGFloat){
        horizontalLine(y: yPos, leftX: leftOffset, rightX: w())
        var offset = h() * sets.lineDistance
        horizontalLine(y: yPos + offset, leftX: leftOffset, rightX: w())
        horizontalLine(y: yPos - offset, leftX: leftOffset, rightX: w())
        offset = h() * sets.lineDistance  * 2
        horizontalLine(y: yPos + offset, leftX: leftOffset, rightX: w())
        horizontalLine(y: yPos - offset, leftX: leftOffset, rightX: w())
    }
    
    private func horizontalLine(y:CGFloat, leftX:CGFloat, rightX:CGFloat){
        drawLine(color: sets.lineColor.cgColor, sPos: CGPoint(x:leftX, y: y), fPos: CGPoint(x:rightX, y: y), depth: h() * sets.lineDepth)
    }
    
    private func drawNote(_ note:NoteData, xPos:CGFloat){
        let noteLineOffset = h() * sets.missingLineLength*0.5
        
        if let drawable = NoteDrawables.getDrawabe(note: note){
            var noteHeight = note.noteHeight()
            var yPos = CGFloat(noteHeight) * sets.lineDistance * 0.5
            var blockCenter:CGFloat = 0
            
            if(note.inBottomBlock()){
                blockCenter = sets.botBlockCenter
            } else {
                blockCenter = sets.topBlockCenter
            }
             yPos += blockCenter
            
            noteHeight *= -1
            
            if noteHeight > 5{
                let missingLines = (noteHeight-6)/2
                for index in 0...missingLines{
                    horizontalLine(y: h()*(blockCenter - sets.lineDistance*CGFloat(3+index)),
                                   leftX: xPos - noteLineOffset,
                                   rightX: xPos + noteLineOffset)
                }
            } else if  noteHeight < -5 {
                let missingLines = (-6 - noteHeight)/2
                for index in 0...missingLines{
                    horizontalLine(y: h()*(blockCenter + sets.lineDistance*CGFloat(3+index)),
                                   leftX: xPos - noteLineOffset,
                                   rightX: xPos + noteLineOffset)
                }
            }
        
            yPos = yPos * h()
            draw(drawable, posX: xPos, posY: yPos, fullSize: h())
            
            if let special = NoteDrawables.getSpecial(note){
                draw(special, posX: xPos + sets.special_offset*h(), posY: yPos, fullSize:h())
            }
        }
    }
}
