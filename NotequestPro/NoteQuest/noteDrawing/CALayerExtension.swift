//
//  CALayerExtension.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 20.03.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation
import UIKit

extension CALayer{
    func drawLine(color:CGColor, sPos:CGPoint, fPos:CGPoint, depth:CGFloat){
        let line = CAShapeLayer()
        let linePath = UIBezierPath()
        linePath.move(to: sPos)
        linePath.addLine(to: fPos)
        line.path = linePath.cgPath
        line.fillColor = nil
        line.lineWidth = depth
        line.strokeColor = color
        addSublayer(line)
    }
    
    func draw(_ drawable:NoteDrawable, posX:CGFloat, posY:CGFloat, size:CGSize){
        let layer = CALayer()
        layer.frame = CGRect(
            x: posX - drawable.dX * size.width,
            y: posY - drawable.dY * size.height,
            width: size.width,
            height: size.height)
        layer.contents = drawable.image.cgImage
        layer.masksToBounds = true
        addSublayer(layer)
    }
    
    func draw(_ drawable:NoteDrawable, posX:CGFloat, posY:CGFloat,fullSize:CGFloat, scale:CGFloat? = nil){
        draw( drawable, posX:posX, posY:posY, size:drawable.calcScaledSize(fullSize: fullSize, scale: scale))
    }
}
