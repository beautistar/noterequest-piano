//
//  NoteEnums.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 20.03.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation

public enum Notes:Int{
    case none = -1
    case A = 5
    case B = 6
    case C = 0
    case D = 1
    case E = 2
    case F = 3
    case G = 4
}

public enum NoteSpecs:String{
    case none = ""
    case sharp = "#"
    case flat = "b"
    case natural = "♮"
}

extension NoteSpecs{
    func isNatural()->Bool{
        return self != .sharp && self != .flat
    }
}

public enum NoteLengths:Double{
    case L16 = 0.0625
    case L8 = 0.125
    case L4 = 0.25
    case L2 = 0.5
    case L1 = 1
}


