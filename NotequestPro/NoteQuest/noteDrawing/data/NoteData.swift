//
//  NoteData.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 20.03.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation

public class NoteData:NSObject{
    public let note:Notes
    public let octave:Int
    public let length:NoteLengths
    public let special:NoteSpecs
    private let blank:NoteBlankBase?
    
    private let inBottom:Bool
    
    public init(note:String, octave:Int, length:NoteLengths, special:String, bottom:Bool = false){
        self.note = NoteData.getNote(note) ?? .none
        self.octave = octave
        self.length = length
        self.special = NoteData.calcSpecial(special) ?? .none
        self.inBottom = bottom
        self.blank = NoteBlanks.getBlank(self.note, spec: self.special)
    }
    
    public init(note:Notes, octave:Int, length:NoteLengths, special:NoteSpecs? = .none, bottom:Bool = false){
        self.note = note
        self.octave = octave
        self.length = length
        self.special = special ?? .none
        self.inBottom = bottom
        self.blank = NoteBlanks.getBlank(self.note, spec: self.special)
    }
    
    public static func empty(length:NoteLengths)->NoteData{
        return NoteData(note: .none, octave: 5, length: length)
    }
    
    public func copy(bottom:Bool? = nil, special:NoteSpecs? = nil, length:NoteLengths? = nil)->NoteData{
        return NoteData(note:note,
                        octave:octave,
                        length:length ?? self.length,
                        special:special ?? self.special,
                        bottom: bottom ?? self.inBottom)
    }
    
    func title()->String{
        var result = ""
        switch note {
        case .A: result += "A"; break
        case .B: result += "B"; break
        case .C: result += "C"; break
        case .D: result += "D"; break
        case .E: result += "E"; break
        case .F: result += "F"; break
        case .G: result += "G"; break;
        default: break
        }
        switch special {
        case .flat: result += "f"; break
        case .sharp: result += "s"; break
        case.natural : result += "n"; break;
        default: break
        }
        
        return result
    }
    func freq()->Double{
        return (blank?.freq ?? 0) * pow(2, octave).doubleValue
    }
    
    public func noteHeight()->Int{
        let result = (octave-1) * 7 + note.rawValue
        if(inBottomBlock()){
            return NoteViewSets.MIDDLE_BOTTOM - result
        } else {
            return NoteViewSets.MIDDLE_TOP - result
        }
    }
    
    public func inBottomBlock()->Bool{
        return inBottom || ((octave-1) * 7 + note.rawValue < 20)
    }
}

extension Decimal {
    var doubleValue:Double {
        return NSDecimalNumber(decimal:self).doubleValue
    }
}


extension NoteData{
    static func parse(_ notes:[String])->[NoteData]{
        var prevNote:NoteData?
        var result = [NoteData]()
        var length = NoteLengths.L1
        if notes.count>1{
            length = .L4
        }
        notes.forEach{
            prevNote = NoteData.parse($0, length: length, prevNote: prevNote)
            if let note = prevNote{
                result.append(note)
            }
        }
        return result
    }
    
    static func parse(_ str:String, length: NoteLengths, prevNote:NoteData? )->NoteData?{
        guard let note = getNote(str.subStr(from:0, to:1)) else {return nil}
    //    guard note != .none else { return NoteData.empty(length: length)}
        let octave = calcOctave(str.subStr(from:1, to: 2))
        if octave == 0 {return nil}
        let isBottom = str.subStr(from:2, to:3) == "2"
        var spec:NoteSpecs?
        if(isBottom){
            spec = calcSpecial(str.subStr(from:3, to:4))
        } else {
            spec = calcSpecial(str.subStr(from:2, to:3))
        }
        
        spec = spec ?? isNormal(prevNote)
        
        return NoteData(note: note, octave: octave, length: length, special: spec, bottom: isBottom)
    }
    
    private static func calcOctave(_ str:String)->Int{
        return Int(str) ?? 0
    }
    
    private static func isNormal(_ prevNote:NoteData?)->NoteSpecs?{
        if(prevNote?.special == .flat || prevNote?.special == .sharp){
            return .natural
        } else{
            return nil
        }
    }
    
    private static func calcSpecial(_ str:String)->NoteSpecs?{
        switch str {
        case "h", "s", "#": return NoteSpecs.sharp
        case "f", "b": return NoteSpecs.flat
        case "n", "♮": return NoteSpecs.natural
        default: return nil;
        }
    }
    
    static func getNote(_ char:String)->Notes?{
        switch char {
        case "A": return .A
        case "B": return .B
        case "C": return .C
        case "D": return .D
        case "E": return .E
        case "F": return .F
        case "G": return .G
        case "N": return Notes.none
        default: return nil
        }
    }
}
