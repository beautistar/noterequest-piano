//
//  NoteBar.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 20.03.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation

public typealias NoteBar = Array<NoteData>

public extension Array where Element: NoteData{
    public func isEmpty() -> Bool {
        return self.count == 0
    }
    
    public func duration()->Double{
        var result:Double = 0
        forEach{ result += $0.length.rawValue }
        return result
    }
}
