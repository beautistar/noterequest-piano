//
//  NoteDrawable.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 20.03.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation
import UIKit

public struct NoteDrawable{
    let image:UIImage
    let dX:CGFloat
    let dY:CGFloat
    let scale:CGFloat?
}

public class NoteDrawables{    
    static func getDrawabe(note:NoteData)->NoteDrawable?{
        if (note.note == .none){
            return NoteViewSets.pause
        }
        
        if(note.length == .L1 || note.length == .L2){
            return NoteViewSets.note1
        }else{
            if (note.noteHeight() <= 0){
                return NoteViewSets.note4b
            } else {
                return NoteViewSets.note4
            }
        }
    }
    
    static func getSpecial( _ note: NoteData)->NoteDrawable?{
        switch(note.special){
        case .flat: return NoteViewSets.flat
        case .natural : return NoteViewSets.natural
        case .sharp: return NoteViewSets.sharp
        case .none: return nil
        }
    }
}

extension NoteDrawable{
    func calcScaledSize(fullSize:CGFloat, scale:CGFloat? = nil)->CGSize{
        let imageSize = self.image.size
        if let scale = scale ?? self.scale{
            if(imageSize.width > imageSize.height){
                return CGSize(
                    width: fullSize * scale,
                    height: fullSize * scale * imageSize.height / imageSize.width)
            } else {
                return CGSize(
                    width: fullSize * scale * imageSize.width / imageSize.height,
                    height: fullSize * scale)
            }
        } else {
            return imageSize
        }
    }
}
