//
//  NoteView.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 20.03.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation
import UIKit

class NoteView:UIView{
    
    private var _layer:NoteViewLayer?
    private var sets = NoteViewSets()
    private var notes: NoteBar?
    
    public func drawNotes(_ notes:NoteBar){
        self.notes = notes
        redraw()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        redraw()
    }
    
    private func redraw(){
        let bounds = calcBounds()
        let newLayer = NoteViewLayer()
        newLayer.initialize(frame: bounds, sets: sets)
        newLayer.drawNotes(notes ?? [])
        
        if let oldLayer = _layer{
            self.layer.replaceSublayer(oldLayer, with: newLayer)
        } else {
            self.layer.insertSublayer(newLayer, at: 0)
        }
        
        _layer = newLayer
    }
    
    private func calcBounds()->CGRect{
    //    let minSize = min(frame.width, frame.height)
   //     return CGRect(x: (frame.width - minSize) * 0.5, y: (frame.height - minSize) * 0.5, width:minSize, height:minSize )
        return CGRect(x:0, y:0, width: frame.width, height:frame.height)
    }
}
