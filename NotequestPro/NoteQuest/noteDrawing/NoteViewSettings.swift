//
//  NoteViewSettings.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 20.03.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation
import UIKit

class NoteViewSets{
    
    static let MIDDLE_BOTTOM = 15
    static let MIDDLE_TOP = 27
    
    var backColor =/* #colorLiteral(red: 1.0, green: 0.0, blue: 0.0, alpha: 0.5)*/ #colorLiteral(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0) 
    var lineColor = #colorLiteral(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
    
    var topBlockCenter      :CGFloat = 0.35
    var botBlockCenter      :CGFloat = 0.7
    var lineDepth           :CGFloat = 0.004
    var lineDistance        :CGFloat = 0.05
    var special_offset      :CGFloat = -0.07
    var missingLineLength   :CGFloat = 0.08
    
    public static var branch = NoteDrawable(image:#imageLiteral(resourceName: "note_branch.png"), dX:0.5, dY:0.5, scale:nil)
    public static var normNote = NoteDrawable(image:#imageLiteral(resourceName: "note_norm.png"), dX:0.5, dY: 0.5, scale: 0.35)
    public static var bassNote = NoteDrawable(image:#imageLiteral(resourceName: "note_bass.png"), dX:0.5, dY: 0.5, scale: 0.19)
    public static var note1 = NoteDrawable(image: #imageLiteral(resourceName: "note_1.png"), dX: 0.5, dY: 0.5, scale: 0.07)
    public static var note4 = NoteDrawable(image: #imageLiteral(resourceName: "note_4.png"), dX: 0.5, dY:0.87, scale: 0.20)
    public static var note4b = NoteDrawable(image: #imageLiteral(resourceName: "note_4_bot.png"), dX: 0.5, dY: 0.12, scale: 0.20)
    
    public static var sharp = NoteDrawable(image: #imageLiteral(resourceName: "note_diez.png"), dX: 0.5, dY: 0.5, scale: 0.09)
    public static var flat = NoteDrawable(image: #imageLiteral(resourceName: "note_flat.png"), dX: 0.5, dY: 0.78, scale: 0.10)
    public static var natural = NoteDrawable(image: #imageLiteral(resourceName: "note_natural.png"), dX: 0.5, dY: 0.5, scale: 0.10)
    
    public static var pause = NoteDrawable(image: #imageLiteral(resourceName: "note_pause.png"), dX: 0.5, dY: 0, scale: 0.07)
}
