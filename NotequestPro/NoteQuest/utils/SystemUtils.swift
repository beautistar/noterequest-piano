//
//  SystemUtils.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 28.02.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation
import UIKit

extension UIDevice {
    static var type: UIUserInterfaceIdiom {
        return UIDevice.current.userInterfaceIdiom
    }
}

func isIpad() -> Bool {
    return UIDevice.type == UIUserInterfaceIdiom.pad
}

func getDeviceType()->String{
    if isIpad() {
        return "2"
    } else {
        return ""
    }
}

var scoreStats = [StatsModel]()

func calcUserScore(data:SessionData, time:TimeInterval, level:Int)->Int{
   var percents = Double(data.succScreens())/Double(data.stepsCompleted())*100
    if(data.succScreens() == 0){
        percents = 0
    }
    var points = scoreFromPercents(Int(percents))
    if(percents >= 80){
        let succScr = data.succScreens()
        switch level {
        case 1:
            points += bonusPts(succScr, 34, 32, 30)
            break;
        case 2:
            points += bonusPts(succScr, 44, 42, 40)
            break;
        case 3:
            points += bonusPts(succScr, 36, 34, 35)
            break;
        case 4, 5:
            points += bonusPts(succScr, 35, 33, 31)
        default: break
        }
    }
   return points
}

private func bonusPts(_ succScr:Int, _ st1:Int, _ st2:Int, _ st3:Int)->Int{
    switch true {
    case succScr >= st1: return 5
    case succScr >= st2: return 3
    case succScr >= st3: return 1
    default: return 0
    }
}

func calcUserScore(pc:Int, tts:TimeInterval, level:Int)->Int{
    return scoreFromPercents(pc)
    
 /*   var score = scoreFromPercents(pc)
    if(score == 0) { return 10 }
    
    switch level {
    case 1:
        if (tts <= 40 && pc >= 92){
            score += 5
        } else if (tts <= 40 && pc >= 80){
            score += 3
        } else if (tts <= 50 && pc >= 80){
            score += 3
        }
        break
    case 2:
        if (tts <= 44 && pc >= 92){
            score += 5
        }  else if (tts <= 50 && pc >= 80){
            score += 3
        }
        break
    case 3, 4, 5:
        if (tts <= 60 && pc >= 92){
            score += 5
        } else if (tts <= 60 && pc >= 80){
            score += 3
        } else if (tts <= 65 && pc >= 80){
            score += 1
        }
        break
    default: break
    }
    return score*/
}


private func scoreFromPercents(_ percents:Int)->Int{
    switch true {
    case percents >= 92: return 20
    case percents >= 80: return 18
    case percents >= 70: return 16
    case percents >= 60: return 14
    case percents >= 50: return 12
    default: return 10
    }

}
