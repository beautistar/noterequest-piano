//
//  ArrayExtensions.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 05.03.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation

extension MutableCollection where Index == Int {
    mutating func shuffle() {
        if count < 2 { return }
        
        for i in startIndex ..< endIndex - 1 {
            let j = Int(arc4random_uniform(UInt32(endIndex - i))) + i
            if i != j {
                self.swapAt(i, j)
            }
        }
    }
}

extension Array {
    func random() -> Element? {
        if self.isEmpty { return nil }
        let randomInt = Int(arc4random_uniform(UInt32(self.count)))
        return self[randomInt]
    }
    
    func random( excludeIndex:Index?) -> Element?{
        if let index = excludeIndex?.value() {
            return random(excludeIndex: Int(index))
        } else {
            return random()
        }
    }
    
    
    func random( excludeIndex: Int)->Element?{
        guard self.isNotEmpty else { return nil}
        guard self.count != 1 else {return self[0] }
        guard (excludeIndex < count) && (excludeIndex >= 0) else { return random() }
       
        let maxLen = UInt32(self.count-1)
        var randomInt = Int(arc4random_uniform(maxLen))
    
        if excludeIndex == randomInt{
            randomInt += 1
        }
        
        return self[randomInt]
    }
    
}
