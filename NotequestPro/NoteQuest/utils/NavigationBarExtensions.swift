//
//  NavigationBarExtensions.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 28.02.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationBar {
    
    func setGradientBackground(colors: [UIColor]) {
        
        var updatedFrame = bounds
        updatedFrame.size.height += self.frame.origin.y
        let gradientLayer = CAGradientLayer(frame: updatedFrame, colors: colors)
        
        setBackgroundImage(gradientLayer.creatGradientImage(), for: UIBarMetrics.default)
    }
}

extension UIViewController{
    func buttons(image:String, text:String, target:Any, selector:Selector)->[UIBarButtonItem]{
        return [
            UIBarButtonItem(image: UIImage(named:image), style: .done, target: target, action: selector),
            UIBarButtonItem(title: text, style: .done, target: target, action: selector)
        ]
    }
}
