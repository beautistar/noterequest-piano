/*
 * Copyright (c) 2018 Mentalstack LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import Foundation
import UIKit
import StoreKit

open class PaymentService {
    private let handler:IPaymentListener
    private var products = [SKProduct]()
    private var dismissed = false
    private var loaded = false
    
    public static func isPayed(_ bundle:ProductIdentifier)->Bool{
        return UserDefaults.standard.bool(forKey: bundle) == true
    }
    
    public init(_ handler:IPaymentListener, payments:Set<ProductIdentifier>) {
        RageProducts.initialize(payments)
        self.handler = handler
        
        NotificationCenter.default.addObserver(self, selector: #selector(PaymentService.handlePurchaseNotification(_:)), name: NSNotification.Name(rawValue: IAPHelper.IAPHelperPurchaseNotification), object: nil)
        self.reloadPayments(false)
    }
    
    public func dismiss(){
        RageProducts.store?.cancel()
        NotificationCenter.default.removeObserver(self)
        dismissed = true
    }
    
    public func pay(_ bundle:ProductIdentifier)->Bool{
        if dismissed || !loaded { return false }
        for product in products{
            if (product.productIdentifier == bundle){
                showAlertView(product)
                return true
            }
        }
        self.reloadPayments()
        return false
    }
    
    public func payFirst()->Bool{
        if dismissed || !loaded { return false }
        for product in products{
            showAlertView(product)
            return true
        }
        self.reloadPayments()
        return false
    }
    
    private func reloadPayments(_ loadCompleted:Bool = true) {
        if(dismissed) { return }
        loaded = false
        self.products.removeAll()
        RageProducts.store.requestProducts(loadPurchaces:loadCompleted){success, products in
            if success, let newProducts = products {
                self.products = newProducts
                self.loaded = true
            }
        }
    }
    
    // MARK: - Purchase Notification
    
    @objc func handlePurchaseNotification(_ notification: Notification) {
        handler.loadingBlock(false)
        if !loaded || dismissed { return }
        guard let productID = notification.object as? String else { return }
        
        for (_, product) in products.enumerated() {
            if product.productIdentifier == productID {
                handler.paymentListener(bundle: productID)
                return
            }
        }
        
        handler.paymentListener(bundle: nil)
    }
    
    private func showAlertView(_ bundle: SKProduct) {
        handler.requestBundle(bundle: bundle){ isSuccess in
            if(isSuccess){
                self.handler.loadingBlock(true)
                RageProducts.store.buyProduct(bundle)
            }
        }
    }
}

