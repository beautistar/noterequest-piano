//
//  TimeHelpers.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 19.02.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation

private let minute:TimeInterval = 60.0
private let second:TimeInterval = 1.0

enum State{
    case PLAYED
    case PAUSED
    case STOPPED
}

class SessionTime:LifeCycleHandler{
    init(fullTime:Int, callback: @escaping ()-> Void){
        self.fullTime = fullTime
        self.listener = callback
    }
    
    func onLifeCyclePause(){
        pause()
    }
    
    func onLifeCycleResume(){
        play()
    }
    
    func start(){
        timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(SessionTime.stepTime), userInfo: nil, repeats: true)
        RunLoop.main.add(timer, forMode: RunLoopMode.commonModes)
        addLifeCycleListener(self)
    }
    
    @objc func stepTime(){
        let currTime = Date.timeIntervalSinceReferenceDate
        if(state == State.PLAYED){
            playedTime += currTime - lastTrackedTime
            listener()
        }
        lastTrackedTime = currTime
    }
    
    private var timer:Timer!
    private var state = State.PLAYED
    private var playedTime:TimeInterval = 0.0
    private let listener: () -> Void
    
    func played()->Bool {
        return state == State.PLAYED
    }
    
    let fullTime:Int
    let startTime = Date.timeIntervalSinceReferenceDate
    private var lastTrackedTime = Date.timeIntervalSinceReferenceDate
    
    func pause(){
        state = State.PAUSED
    }
    
    func stop(){
        removeLifeCycleListener(self)
        state = State.STOPPED
        timer.invalidate()
    }
    
    func play(){
        lastTrackedTime = Date.timeIntervalSinceReferenceDate
        state = State.PLAYED
    }
    
    func timeFromStart()->TimeInterval{
        return playedTime
    }
    
    func strFromStart()->String{
        return (Double(fullTime) - playedTime).shortTextDesc()
    }
    
    func secondsFromStart()->Int{
        return Int(playedTime)
    }
}

typealias TimeInterval = Double
extension TimeInterval{
    private func strFromStart() -> String{
        let elapsedTime: TimeInterval = Date.timeIntervalSinceReferenceDate - self
        return elapsedTime.shortTextDesc()
    }
    
    func shortTextDesc() -> String{
        let strMinutes = String(format: "%02d", minutes())
        let strSeconds = String(format: "%02d", seconds())
        
        return "\(strMinutes):\(strSeconds)"
    }
    
    func minutes()->Int{
        return Int(Double(lround(self)) / minute)
    }
    
    func seconds()->Int{
        return lround(self) % lround(minute)
    }
}
