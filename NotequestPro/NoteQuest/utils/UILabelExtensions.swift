//
//  UILabelExtensions.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 09.03.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation
import UIKit

extension UILabel{
    func adjustTextHeight()
    {
        self.translatesAutoresizingMaskIntoConstraints = true
        self.lineBreakMode = NSLineBreakMode.byWordWrapping
        self.sizeToFit()
    }
}
