//
//  ImageUtils.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 28.02.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation

import UIKit
import Foundation

extension UIImageView{
    func setBack(_ back:UIImage?){
        if let back = back {
            self.image = back
            alpha = 0.8
        }
    }
}
