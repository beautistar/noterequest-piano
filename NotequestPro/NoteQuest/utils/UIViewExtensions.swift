//
//  UIViewExtensions.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 28.02.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func loadViewFromNib(with classToLoad: AnyClass) {
        let bundle = Bundle(for: classToLoad)
        let nib = UINib(nibName: String(describing: classToLoad), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(view)
    }
    
    func setGradientBackground(_ colors: [UIColor], oldLayer:CALayer? = nil)->CALayer {
        var updatedFrame = self.frame
        updatedFrame.origin.y = 0
        updatedFrame.size.height += self.frame.origin.y
        let gradientLayer = CAGradientLayer(frame: updatedFrame, colors: colors)
        if let oldLayer = oldLayer{
            layer.replaceSublayer(oldLayer, with: gradientLayer)
        } else {
            layer.insertSublayer(gradientLayer, at: 0)
        }
        return gradientLayer
    }
    
    func addShadowAndCorners( back:UIImageView?){
        back?.layer.cornerRadius = 8
        back?.layer.masksToBounds = true
        
        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowOffset = CGSize(width: 1, height: 1)
        layer.shadowOpacity = 0.5
        layer.shadowRadius = 3.0
        clipsToBounds = false
    }
    
    func roundCorners(){
        layer.cornerRadius = min(frame.width, frame.height)/2
        layer.masksToBounds = true
    }
}
