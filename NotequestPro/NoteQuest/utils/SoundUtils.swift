//
//  SoundUtils.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 02.03.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation
import AVFoundation
import AVKit

func checkRealNote(checked:Double, needed:Double)->Bool{
    return abs(checked - needed)/needed <= SoundSets.REAL_NOTE_DISTANCE
}

class SoundPlayer:NSObject{
    var player : AVAudioPlayer?

    init(_ soundName:String){
        if let path = Bundle.main.path(forResource: soundName, ofType:"mp3"){
            let url = URL(fileURLWithPath: path)
            player = try? AVAudioPlayer(contentsOf: url )
        }
    }
    
    func play(){
        guard let player = player else { return }
        
        try? AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
        try? AVAudioSession.sharedInstance().setActive(true)
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone){
            player.volume = 0.5
        }
        player.delegate = self
        player.prepareToPlay()
        player.play()
    }
}

extension SoundPlayer:AVAudioPlayerDelegate{
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        try? AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayAndRecord)
    }
}
