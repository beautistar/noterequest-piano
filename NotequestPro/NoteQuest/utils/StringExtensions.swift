//
//  StringExtensions.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 06.03.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation
import UIKit
extension String{
    func subStr(to:Int)->String{

        return NSMutableString.init(string: self).substring(to: min(to, self.count)) as String
    }
    
    func subStr(from:Int)->String{
        return NSMutableString.init(string: self).substring(from: min(from, self.count)) as String
    }
    
    func subStr(from:Int, to:Int)->String{
        return self.subStr(from: from).subStr(to: to-from)
    }
    
    func replace( from:String, to:String)-> String{
        return self.replacingOccurrences(of: from, with: to)
    }
    
    func removeDigits()->String{
        return self.components(separatedBy: CharacterSet.decimalDigits).joined()
    }
    
    func toDouble()->Double { return Double(self) ?? (self as NSString).doubleValue}
}

extension String{
    func convertHtml() -> NSAttributedString{
        guard let data = data(using: .unicode) else { return NSAttributedString() }
        do{
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html],
                                          documentAttributes: nil)
        }catch{
            return NSAttributedString()
        }
    }
    
    func formatToNote()->NSAttributedString{
        var result:NSMutableString = "  <p align=\"center\"><font face=\"Ubuntu\" size=\"10\" color=\"#f42d7d\"><b>"
        for symbol in self{
            switch(symbol){
            case "f": result.append("<i>b</i>"); break
            case "s": result.append("<i>#</i>"); break
            case "n": result.append(""); break
            default : result.append( "\(symbol)")
            }
        }
        
        result.append("  </b></font></p>")
        
        guard let data = (result as String).data(using: .unicode) else { return NSAttributedString() }
        
        do{
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
        }catch{
            return NSAttributedString()
        }
    }
    
    func findFirst(charSet:String, skip:Int)->Int{
        if(skip<=0){
            return findFirst(charSet: charSet)
        } else {
            var str = self
            var skipped = 0
            for _ in 0...skip{
                let pos = str.findFirst(charSet: charSet)
                if(pos >= 0){
                    str = str.subStr(from: pos+1)
                    skipped += pos+1
                } else{
                    return -1
                }
            }
            return skipped-1
        }
    }
    
    func findFirst(charSet:String)->Int{
        var count = 0
        for char in self{
            if let _ = charSet.index(of: char){
                return count
            }
            count += 1
        }
       return -1
    }
}
extension Character{
    func isDigit()->Bool{
        if let scalar = String(self).unicodeScalars.first{
            return CharacterSet.decimalDigits.contains( scalar )
        } else {
            return false
        }
    }
}


extension String {
   
    func findFirstDigitIndex()->Int{
         var cnt = 0
        for char in self{
            if(char.isDigit()){
                return cnt
            }
            cnt += 1
        }
        return -1
    }
}


