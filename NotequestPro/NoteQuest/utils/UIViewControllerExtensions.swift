//
//  UIViewControllerExtensions.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 01.03.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController{
    @objc func onKeyboardHide(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let viewHeight = self.view.frame.height
            self.view.frame = CGRect(x: self.view.frame.origin.x,
                                     y: self.view.frame.origin.y,
                                     width: self.view.frame.width,
                                     height: viewHeight + keyboardSize.height)
        } else {
            debugPrint("We're about to hide the keyboard and the keyboard size is nil. Now is the rapture.")
        }
    }


   @objc func onKeyboardShow(notification: Notification) {
    if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue,
        let window = self.view.window?.frame {
        // We're not just minusing the kb height from the view height because
        // the view could already have been resized for the keyboard before
        self.view.frame = CGRect(x: self.view.frame.origin.x,
                                 y: self.view.frame.origin.y,
                                 width: self.view.frame.width,
                                 height: window.origin.y + window.height - keyboardSize.height)
    } else {
        debugPrint("We're showing the keyboard and either the keyboard size or window is nil: panic widely.")
    }
    }
    
    func popup(_ screen:String, storyboard:String = Storyboards.main){
        if storyboard == Storyboards.alerts &&
            isIpad() &&
            Storyboards.customIpadElements.contains(screen){
            let storyboard = UIStoryboard(name: Storyboards.alerts_Ipad, bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: screen)
            present(controller, animated: true)
            return
        }
        
       let storyboard = UIStoryboard(name: storyboard, bundle: nil)
       let controller = storyboard.instantiateViewController(withIdentifier: screen)
       present(controller, animated: true)
    }
    
    func screen(_ screen:String, storyboard:String = Storyboards.main){
        if(storyboard == Storyboards.main &&
            isIpad() &&
            Storyboards.customIpadElements.contains(screen)){
            let board = UIStoryboard(name: Storyboards.main_Ipad, bundle:nil)
            let controller:UIViewController = board.instantiateViewController(withIdentifier: screen)
            navigationController?.pushViewController(controller, animated: true)
            return
        }
        let storyboard = UIStoryboard(name: storyboard, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: screen)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func switchTo(_ screenID:String){
        let pos = (navigationController?.viewControllers.count ?? 1) - 1
        screen(screenID)
        navigationController?.viewControllers.remove(at: pos)
    }
    
    func initWindow(_ back:UIImageView?, title:String, backTitle:String? = nil){
        
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        navigationController?.navigationBar.setGradientBackground(colors: Colors.navigationGradient)
        self.title = title
        back?.setBack(getBackground())
        
        if let backTitle = backTitle{
            navigationItem.backBarButtonItem = UIBarButtonItem(title: backTitle, style: .plain, target: nil, action: nil)
        }
        if let subviews = navigationController?.navigationBar.subviews{
        for subview in subviews{
            subview.isExclusiveTouch = true
        }
        }
    }
}

extension UIViewController{
    func alert(_ message:String?){
        if let msg = message{
            DispatchQueue.main.async(execute: {
                (UIApplication.shared.delegate as? AppDelegate)?.showAlertWithTitle("", message:msg)
            })
        }
    }
}

extension AppDelegate{
    func showAlertWithTitle(_ title:String, message:(String))   {
        guard let navigation = UIApplication.shared.windows[0].rootViewController as? UINavigationController else {return}
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert )
        let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {Void in})
        alert.addAction(action)
       navigation.present(alert, animated: true, completion:nil)
    }
}
protocol UINavigationListener:class{
    func onScreenClosed(_ screen:String)
}

protocol UINavigationElement:class{
    func navigationTag()->String
}

private var listeners = [UINavigationListener]()

extension UINavigationListener{
    func listenNavigation(){
        if let index = listeners.index(where: {$0 === self}){
            listeners.remove(at: index)
        }
        listeners.append(self)
    }
    
    func removeListenNavigation(){
        if let index = listeners.index(where: {$0 === self}){
            listeners.remove(at: index)
        }
    }
}

extension UINavigationElement{
    func notifyWindow(){
     listeners.forEach{ $0.onScreenClosed(navigationTag()) }
    }
}
