//
//  FreqModule.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 20.02.2018.
//  Copyright © 2018 Ramesh. All rights reserved.
//

import Foundation

class FrequencyApproximator{
    private let NON_FREQ:Double = -10000.0
    
    private let distance: Int
    private var frequences = [Double]()
    private var _freq:Double
    
    init(_ distance:Int) {
        self.distance = distance
        _freq = NON_FREQ
    }
    
    func add(_ freq:Double){
        if(frequences.count == distance){
            frequences.remove(at: 0)
        }
        
        frequences.append(freq)
        
        if(frequences.count < Int(Double(distance) * 0.5)){
            _freq = NON_FREQ
        } else {
            var sum = 0.0
            for freqCell in frequences{
                sum = sum + freqCell
            }
            _freq = sum/Double(frequences.count)
            
            for freqCell in frequences{
                if(abs(freqCell - _freq) > _freq*0.03){
                    _freq = NON_FREQ
                    return
                }
            }
        }
    }
    
    func freq() -> Double { return _freq }
    func intFreq() -> Int { return Int(_freq) }
}


