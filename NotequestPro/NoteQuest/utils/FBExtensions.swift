//
//  FBExtensions.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 07.03.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation
import UIKit
import FBSDKShareKit

extension UIViewController{
    func shareToFB(_ text: String, btnFB:UIButton, innerView:UIView)->FBSDKShareButton  {
        let content : FBSDKShareLinkContent = FBSDKShareLinkContent()
        content.contentURL = AppSets.FB_URL
        content.quote = text
        let button : FBSDKShareButton = FBSDKShareButton()
        button.shareContent = content
        button.setTitle("", for: .normal)
   //     button.setImage(#imageLiteral(resourceName: "btn-share"), for: .normal)
     //   button.frame = CGRect(x:0,y:0, width:innerView.frame.width, height:innerView.frame.height)
        button.frame = btnFB.frame
        button.roundCorners()
        innerView.addSubview(button)
        
        return button
    }
}
