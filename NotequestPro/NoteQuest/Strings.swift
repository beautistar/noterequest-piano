//
//  Strings.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 01.03.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation

class str {
    static let app_name = "Note Quest"
    static let default_name = "User"
    static let sound_check = "Soundcheck"
    static let virtual_piano = "Virtual Mode"
    static let real_mode = "Real Mode"
    static let back = " Back"
    static let load_data_error = "data loading problems..."
    static let levels = "Levels"
    static let level_desc = "Level %s"
    static let skip = "Skip"
    static let info = "Info"
    static let virtual_mode_lock_desc = "This level is not available from Virtual Mode"
    static let coming_soon = "Coming soon!"
    static let payment_desc = "Upgrade now to Note Quest Pro and get all levels, including all new features coming soon!"
    static let payments_mot_loaded = "Unable to connect, Please try again later."
    static let bad_internet = "Please check Internet Connection"
    static let yes = "yes"
    static let score = "score"
}
