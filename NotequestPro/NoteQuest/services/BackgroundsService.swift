//
//  BackgroundsService.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 28.02.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation
import UIKit

class BackgroundImage{
    let iconID:String
    let name:String
    let back:String
    init(icon:String, name:String, back:String) {
        self.iconID = icon
        self.name = name
        self.back = back
    }
}
let defaultBackground:BackgroundImage = BackgroundImage(icon:"ic-grad.png", name:"Classic", back: "bg-grad.png" )

let backgrounds:[BackgroundImage] = [
    defaultBackground,
    BackgroundImage(icon:"ic-beach.png", name:"Beach", back:"bg-beach.png"),
    BackgroundImage(icon:"ic-park.png", name:"Park", back:"bg-park.png"),
    BackgroundImage(icon:"ic-city.png", name:"City", back:"bg-city.png"),
    BackgroundImage(icon:"ic-game.png", name:"Video Game", back:"bg-game.png")]


func getBackground()->UIImage?{
    return getBackground(backgroundID())
}

func backgroundID() -> BackgroundImage{
    return searchBackByName(UserSets.backgroundID()) ?? defaultBackground
}

private func searchBackByName(_ name:String?)->BackgroundImage?{
    for image in backgrounds {
        if (image.name == name){
            return image
        }
    }
    
    return nil
}
func getBackgroundIcon(_ resource:BackgroundImage)->UIImage?{
    return UIImage(named:resource.iconID) ?? nil
}

func getBackground(_ resource:BackgroundImage)->UIImage?{
    return UIImage(named:resource.back) ?? nil
}

func setBackground(_ resource:BackgroundImage){
   UserSets.backgroundID(resource.name)
}
