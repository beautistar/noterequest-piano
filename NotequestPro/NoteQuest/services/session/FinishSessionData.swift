//
//  FinishSessionData.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 06.03.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation

enum FinishSessionResult{
    case REPEAT
    case CHOOSE_LEVEL
    case CANCEL
    case SHARE
    case NONE
}

class FinishSessionData{
    
    static var lastFinishedData:FinishSessionData?
    
    let time:TimeInterval
    let scores:SessionData
    var state:FinishSessionResult
    
    init(time:TimeInterval, data:SessionData) {
        self.time = time
        self.scores = data
        self.state = FinishSessionResult.NONE
    }
}
