
//
//  LeveGenerator.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 22.03.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation

class LevelGenerator{
    internal static func parseParts(_ rawParts:[[String]])->[NoteCell]{
        return rawParts.map{ return NoteCell(NoteData.parse($0)) }
    }
    
    static func generate(_ level:Int)-> [NoteCell]{
        switch level {
        case 1: return notes(bottom:18, top:26, count: 60, notesInList: 1, includeSpecs: false)
        case 2: return notes(bottom: 15, top: 29, count: 80, notesInList: 1, includeSpecs: false)
        case 3: return shuffle(LevelsNotes.level3Sets, count: 90)
        case 4: return shuffle(LevelsNotes.level4Sets, count: 90)
        case 5: return shuffle(LevelsNotes.level5Sets, count: 90)
        case 1001: return landmarkMode(["C4", "F3", "G4"], length: 200 )
        case 1002: return landmarkMode(["C3", "C4", "C5"], length: 200 )
        case 1003: return landmarkMode(["C2", "C3", "C4", "C5", "C6"], length: 200 )
        case 1004: return landmarkMode(["C2", "C3", "C4", "C5", "C6", "F3", "G4"], length: 300)
        case 1102: return shuffle(LevelsNotes.interval_2_1) + shuffle(LevelsNotes.interval_2_2)
        case 1103: return shuffle(LevelsNotes.interval_3_1)
        default: return shuffle(parseParts([["C4"]]), count: 1)
        }
    }
    
    private static func landmarkMode(_ rawNotes:[String], length:Int)->[NoteCell]{
        var prevNote:NoteData?
        var list = [NoteData]()
        for rawNote in rawNotes{
            prevNote = NoteData.parse(rawNote, length: .L1, prevNote: prevNote)
            list.append(prevNote!)
        }
        
        return notes(parts: list, count: length, notesInList: 1, includeSpecs: false)
    }
    
    private static func shuffle(_ list:[NoteCell])->[NoteCell]{
        var levels = list + []
        levels.shuffle()
        return levels
    }
    
    private static func shuffle(_ parts:[NoteCell], count:Int)->[NoteCell]{
       var prev:NoteCell?
       var result = [NoteCell]()
        (1...count).forEach{ _ in
            
            let index = parts.index{ elt -> Bool in
                return prev == elt
            }
            if let new = parts.random(excludeIndex: index){
                prev = new
                result.append(new.copy())
            } else {
                print("oops")
            }
        }
        return result
    }
    
    private static func notes(parts:[NoteData], count: Int, notesInList:Int, includeSpecs:Bool)->[NoteCell]{
        guard parts.count>1 else {
            print("not have variants from current settings")
            return []
        }
        
        var prev: NoteData?
        var result = [NoteCell]()
        var len = NoteLengths.L1
        if(notesInList > 1){
            len = .L4
        }
        
        for _ in 1...count{
            var currParts = [NoteData]()
            for _ in 1...notesInList{
                var newElt:NoteData!
                if let prev = prev{
                    let index = parts.index(of:prev)?.value() ?? -1
                    newElt = parts.random(excludeIndex: Int(index))
                } else {
                    newElt = parts.random()
                }
                if(newElt.note == .G && newElt.octave == 4){
                    print("oops")
                }
                
                if(currParts.count > 0 && prev?.special.isNatural() == false && newElt.special.isNatural() == true){
                    currParts.append(newElt.copy(bottom: nil, special: .natural, length:len))
                } else {
                    currParts.append(newElt!.copy(bottom:nil, special: nil, length:len))
                }
                prev = newElt
            }
            result.append(NoteCell(currParts))
        }
        
        return result
    }
    
    private static func notes(bottom:Int, top:Int, count: Int, notesInList:Int, includeSpecs:Bool)->[NoteCell]{
        let parts = constructParts(bottom: bottom, top: top, includeSpecs: includeSpecs)
        return notes(parts:parts, count: count, notesInList:notesInList, includeSpecs:includeSpecs)
    }
    
    private static func constructParts(bottom:Int, top:Int, includeSpecs:Bool)->[NoteData]{
        var result = [NoteData]()
        for index in bottom...top{
            let blank = NoteBlanks.list[ (index - 1) % NoteBlanks.list.count ]
            let octave = Int((index-1) / 7) + 1
            let p = part(blank.base, octave: octave)
            result.append( p )
            if (index == 22){
                result.append(p.copy(bottom:true))
            }
            if(includeSpecs){
                if let sharp = blank.sharp {
                    result.append( part(sharp, octave: octave) )
                }
                if let flat = blank.flat {
                    result.append( part(flat, octave: octave) )
                }
            }
        }
        return result
    }
    
    private static func part(_ blank:NoteBlankBase, octave:Int)->NoteData{
        return NoteData(note: blank.title.subStr(from: 0, to: 1), octave: octave, length: .L1, special: blank.title.subStr(from: 1, to: 2))
    }
}
