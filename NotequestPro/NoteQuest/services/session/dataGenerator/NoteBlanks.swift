//
//  NoteData.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 22.03.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation

internal class NoteBlanks{
    static let C  = NoteBlankBase(title: "C", freq: 16.3516)
    static let Cs = NoteBlankBase(title: "C#", freq: 17.3239)
    static let Db = NoteBlankBase(title: "Db", freq: 17.3239)
    static let D  = NoteBlankBase(title: "D", freq: 18.3540)
    static let Ds = NoteBlankBase(title: "D#", freq: 19.4454)
    static let Eb = NoteBlankBase(title: "Eb", freq: 19.4454)
    static let E  = NoteBlankBase(title: "E", freq: 20.6017)
    static let F  = NoteBlankBase(title: "F", freq: 21.8268)
    static let Fs = NoteBlankBase(title: "F#", freq: 23.1247)
    static let Gb = NoteBlankBase(title: "Gb", freq: 23.1247)
    static let G =  NoteBlankBase(title: "G", freq: 24.4997)
    static let Gs = NoteBlankBase(title: "G#", freq: 25.9565)
    static let Ab = NoteBlankBase(title: "Ab", freq: 25.9565)
    static let A  = NoteBlankBase(title: "A", freq: 27.50)
    static let As = NoteBlankBase(title: "A#", freq: 29.1352)
    static let Bb = NoteBlankBase(title: "Bb", freq: 29.1352)
    static let  B = NoteBlankBase(title: "B", freq: 30.8677)

    static let list = [
        NoteBlank(base:C, sharp: Cs, flat:nil),
        NoteBlank(base:D, sharp: Ds, flat:Db),
        NoteBlank(base:E, sharp: nil, flat: Eb),
        NoteBlank(base:F, sharp: Fs, flat: nil),
        NoteBlank(base:G, sharp: Gs, flat:Gb),
        NoteBlank(base:A, sharp: As, flat: Ab),
        NoteBlank(base:B, sharp: nil, flat: Bb)
    ]
    
    static func getBlank(_ note:Notes, spec:NoteSpecs?)->NoteBlankBase?{
        for blank in list{
            let char = blank.base.title.subStr(to: 1)
            if NoteData.getNote(char) == note{
                let spec = spec ?? .none
                switch spec{
                case .flat: return blank.flat
                case .sharp: return blank.sharp
                default: return blank.base
                }
            }
        }
        return nil
    }
}

internal struct NoteBlankBase{
    let title:String
    let freq:Double
}

internal struct NoteBlank{
    let base:NoteBlankBase
    let sharp:NoteBlankBase?
    let flat:NoteBlankBase?
}
