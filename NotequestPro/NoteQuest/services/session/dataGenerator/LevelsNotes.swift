//
//  LevelsNotes.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 04.04.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation

class LevelsNotes{
    static var level3Sets = LevelGenerator.parseParts( [
        ["C4", "E4"],["D3", "F3"],["C5", "G4"],["E3", "B32"],
        ["B3", "C4"],["F4", "C4"],["F3", "B2"],["F5", "D5"],
        ["D42","A3"],["D4", "F4"],["D3", "B2"],["D4", "D5"],
        ["E4", "C5"],["B4", "G4"],["C3", "B2"],["C4", "E4"],
        ["F5", "E5"],["G3","C42"],["C5", "B4"],["B4", "F4"],
        ["A2", "G3"],["A3","C42"],["G4", "A4"],["E5", "E4"],
        ["G3", "C3"],["C4", "G4"],["B3", "E4"],["D5", "A4"],
        ["E3", "C3"],["C5", "D5"],["G2", "F3"],["C3", "A2"],
        ["D4", "A4"],["C3","C42"],["F4", "D5"],["F4", "D4"]
        ])
    
    static let level4Sets = LevelGenerator.parseParts([
        ["E4f", "N"], ["A4f", "N"], ["F4s", "N"], ["F3s", "N"], ["G3f", "N"],
        ["D4", "D4s"], ["D3s", "N"], ["F4", "B4f"], ["B2", "N"], ["B3f", "G4"],
        ["B4", "G4s"], [ "E4f", "N"], ["C42s", "N"], ["A2", "C3s"],["G3f", "N"],
        ["E5", "N"], [ "A4f", "N"], ["F3", "N"], ["E4f", "C4"],["A3f", "A3"],
        ["C42", "E3f"],["D5s", "N"], ["F3s", "G3"], ["F4", "C4s"],
        ["D5", "N"], ["F4", "A4f"], ["A3f", "N"], ["A4", "A4b"], ["B2f", "N"],
        ["F3s", "N"], ["G4", "E4f"], ["C5s", "C5"],["D4", "N"],
        ["B32f", "C42"], ["G2s", "N"],
        ["D42", "A3"], ["B4", "F4s"],["B3f", "E4f"], ["G3s", "G4s"]
        ])
    
    static let level5Sets = LevelGenerator.parseParts([
        ["G5", "N"], ["B4b", "N"], ["E2", "N"], ["C2", "N"],
        ["A5", "N"], ["F5s", "A5"], ["A2", "D2"], ["G4f", "N"],["F3", "F2"],
        ["E5", "B5f"], ["D42", "N"], ["F4s", "N"], ["C5", "D4"], ["E42", "N"],
        ["B32", "D3"], ["C5", "A5"], ["B4", "D5s"],["D2", "N"],
        ["A4", "B3"],["C5", "N"], ["G3f", "N"], ["B32f", "B32"], ["C5", "N"],
        ["E2", "N"],["A2", "G3s"], ["A4f", "A5f"], ["D4", "E4f"],
        ["F4", "B3f"], ["G2f", "N"], ["B3", "B4"],["E42", "N"],
        ["G2s", "D3s"],["D42s", "N"],["A5", "N"],["B3f","F4"],["C3", "N"],
        ["C2", "C3"],["E5f","B5f"],["D42","F3s"],["B3", "A4f"],["A3","F4s"]
        ])
    
    static let interval_2_1 = LevelGenerator.parseParts([
        ["C4", "D4"], ["G4", "F4"],["C42","B32"], ["C4", "B3"], ["G3", "F3"],
        ["F3", "E3"], ["G4", "A5"], ["C5", "D5"], ["C5", "B4"], ["C3", "D3"], ["C3", "B2"]
        ])
    
    static let interval_2_2 = LevelGenerator.parseParts([
        ["G2", "A2"], ["G2", "F2"], ["E5", "F5"], ["G5", "F5"], ["E4", "F4"],
        ["E4", "D4"], ["C5", "B4"], ["D3", "E3"], ["A2", "B2"], ["G5", "A5"],
        ["F5", "E5"],["C42","D42"], ["C2", "D2"], ["C6", "B5"], ["B4", "A4"]
        ])
    
    static let interval_3_1 = LevelGenerator.parseParts([
        ["C4", "E4"], ["E4", "C4"], ["G4", "B4"], ["G4", "E4"],["C42", "A3"],
        ["A3", "F3"], ["F3", "D3"], ["C5", "A4"], ["C3", "E3"], ["C3", "A2"], ["C5", "E5"]
        ])
}
