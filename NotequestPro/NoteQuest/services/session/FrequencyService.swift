//
//  FrequencyService.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 02.03.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation
import AudioKit

protocol IFrequenceListener{
    func listenFrequency(freq:Double)
}

class FrequencyService{
    private let listener:IFrequenceListener
    
    private var mic: AKMicrophone?
    private var tracker: AKFrequencyTracker?
    private var silence: AKBooster?
    private let freq = FrequencyApproximator(SoundSets.FREQ_DISTANCE)
    private var timer:Timer?
    private var stopped = false
    
    init(_ listener:IFrequenceListener) {
        self.listener = listener
         AKSettings.audioInputEnabled = true
        
        let microphone = AKMicrophone()
        mic = microphone
        tracker = AKFrequencyTracker.init(microphone)
        silence = AKBooster(tracker, gain: 0)
        AudioKit.output = silence
    }
    
    func start(){
        if(stopped){
           return
        }
        addLifeCycleListener(self)
        
       mic?.start()
        tracker?.start()
        silence?.start()
     
        try? AudioKit.start()
        
        timer?.invalidate()
        timer = Timer.scheduledTimer(timeInterval: SoundSets.FREQ_INTERVAL, target: self, selector: #selector(FrequencyService.timeInterval), userInfo: nil, repeats: true)
    }
    
    func stop(){
        if(stopped){
            return
        }
       
        pause()
        mic?.stop()
        removeLifeCycleListener(self)
         stopped = true
    }
    
    func pause() {
        if(stopped){
            return
        }

        tracker?.stop()
        silence?.stop()
        timer?.invalidate()
        timer = nil
        do {
            try AudioKit.stop()
        } catch{
            print(error)
        }
    }
    
    @objc private func timeInterval(){
        if let tracker = tracker{
       //     print(tracker.amplitude, tracker.frequency)
            guard tracker.amplitude < SoundSets.MIN_AMPLITUDE else { return }
            freq.add(tracker.frequency)
            let frequency = freq.freq()
            if(frequency > 0){
                listener.listenFrequency(freq: frequency)
            }
        }
    }
}

extension FrequencyService:LifeCycleHandler{
    func onLifeCyclePause() {
       pause()
    }
    
    func onLifeCycleResume() {
        start()
    }
}
