//
//  UserDataService.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 28.02.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation

fileprivate struct keys{
    static let back = "backgroundID"
    static let name = "userName"
    static let level = "levelID"
    static let mode = "modeID"
}

fileprivate class UserData{
    var background:String?
    var name:String?
    var level:Int
    var mode:QuestMode
    
    fileprivate init(){
        let std = UserDefaults.standard
        name = std.string(forKey: keys.name)
        level = std.integer(forKey: keys.level)
        if(level == 0){
            level = 1
        }
        mode = modeByName( std.string(forKey: keys.mode) ?? "")
    }
    
    fileprivate func store(){
        let std = UserDefaults.standard
        std.set(background, forKey: keys.back)
        std.set(name, forKey: keys.name)
        std.set(level, forKey: keys.level)
        std.set(modeName(mode), forKey: keys.mode)
        std.synchronize()
    }
}

class UserSets{
    static func notSetted()->Bool { return data().name == nil }
    
    static func backgroundID()->String?{ return data().background }
    static func backgroundID(_ value:String){ data().background = value }
    
    static func name(value:String) { data().name = value }
    static func name(_ defVal:String = str.default_name) -> String { return data().name ?? defVal }
    
    static func level(_ value: Int){ data().level = value }
    static func level()->Int { return data().level }
    
    static func mode()->QuestMode { return data().mode}
    static func mode(_ value:QuestMode) { data().mode = value }
    
    static func store(){
        data().store()
    }
    
    static private var _data:UserData?
    static private func data()->UserData{
        if let data = _data {
            return data
        } else {
            let data = UserData()
            _data = data
            return data
        }
    }
}
