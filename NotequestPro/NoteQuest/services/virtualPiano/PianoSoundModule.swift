//
//  PianoSoundModule.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 21.02.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation
import AudioToolbox
import MessageUI
import AudioKit

class PianoSoundModule{
    private var frequencyArray = [Int]()
    private var audioArray : NSMutableArray = []
    private var player:AVAudioPlayer! = nil
    
    init() {
        let plistPath = Bundle.main.path(forResource: "keyboardLayout", ofType: "plist")
        let names = NSArray(contentsOfFile: plistPath!) as! Array<String>
        
        let blackKeysArray : NSMutableArray = []
        let whiteKeysArray : NSMutableArray = []
        let combineKeysArray : NSMutableArray = []
        
        let freqPlistPath = Bundle.main.path(forResource: "NotesFrequency", ofType: "plist")
        let freqNames = NSArray(contentsOfFile: freqPlistPath!) as! Array<String>
        
        var blackKeysFreqArray = [Int]()
        var whiteKeysFreqArray = [Int]()
        
        for i in 15 ..< 64 {
            let strName = names[i]
            let strFreq = freqNames[i]
            let myFreq = Int(Double(strFreq) ?? 0.0)
            
            if (strName.hasSuffix("s")) {
                blackKeysArray.add(names[i])
                blackKeysFreqArray.append(myFreq)
            } else {
                whiteKeysArray.add(names[i])
                whiteKeysFreqArray.append(myFreq)
            }
        }
        
        frequencyArray.append(contentsOf: whiteKeysFreqArray)
        frequencyArray.append(contentsOf: blackKeysFreqArray)
        combineKeysArray.addObjects(from: whiteKeysArray as [AnyObject])
        combineKeysArray.addObjects(from: blackKeysArray as [AnyObject])
        for i in 0 ..< combineKeysArray.count {
            var soundID : SystemSoundID = 0
            let path = Bundle.main.path(forResource: (combineKeysArray[i] as! String), ofType: "aif")
            AudioServicesCreateSystemSoundID(URL(fileURLWithPath:path!) as CFURL, &soundID)
            let scanSoundURL = NSURL(fileURLWithPath: path!)
            audioArray.add(scanSoundURL)
        }
    }
    
    func getHintKeys(notes: [Int])->[Int]{
        var result = [Int]()
        for note in notes{
            for key in frequencyArray{
                if(abs(note - key)<=1){
                    if let keyID = frequencyArray.index(of: key){
                        result.append(keyID+KEYBOARD_TAG_OFFSET)
                    }
                }
            }
        }
      return result
    }
    
    func finish(){
        player?.stop()
        try? AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayAndRecord)
    }
    
    private var prevCategory:String?
    func playSound(tag:Int)->Double?{
        let imgTag = tag-KEYBOARD_TAG_OFFSET // dont know why
        if audioArray.count > imgTag {
            prevCategory = AVAudioSession.sharedInstance().category
            try? AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try? AVAudioSession.sharedInstance().setActive(true)
            player = try? AVAudioPlayer(contentsOf: audioArray[imgTag] as! URL )
            if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone){
                player?.volume = 0.5
            }
            player?.prepareToPlay()
            player?.play()
            return Double(frequencyArray[imgTag])
        } else {
            return nil
        }
    }
}
