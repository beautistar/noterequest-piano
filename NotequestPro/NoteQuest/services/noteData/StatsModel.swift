//
//  StatsModel.swift
//  NoteQuest
//
//  Created by Yin on 2018/8/13.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import UIKit

class StatsModel: NSObject, NSCoding {
    
    //var id: Int = 0
    var user_name: String = ""
    var level: String = ""
    var total_rounds: Int = 0
    var top_points: Int = 0
    var stars: Int = 0
    
    init(/*id: Int, */user_name: String, level: String, total_round: Int, top_points: Int,  stars: Int) {
        
        //self.id = id
        self.user_name = user_name
        self.level = level
        self.top_points = top_points
        self.total_rounds = total_round
        
    }
    
    func encode(with aCoder: NSCoder) {
        
        //aCoder.encode(id, forKey: "s_id")
        aCoder.encode(user_name, forKey: "s_name")
        aCoder.encode(level, forKey: "s_level")
        aCoder.encode(top_points, forKey: "s_top_point")
        aCoder.encode(total_rounds, forKey: "s_total_round")
        aCoder.encode(stars, forKey: "s_stars")
        
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        
        //let id = aDecoder.decodeInteger(forKey: "s_id")
        let name = aDecoder.decodeObject(forKey: "s_name") as! String
        let level = aDecoder.decodeObject(forKey: "s_level") as! String
        let top_point = aDecoder.decodeInteger(forKey: "s_top_point")
        let total_round = aDecoder.decodeInteger(forKey: "s_total_round")
        let stars = aDecoder.decodeInteger(forKey: "s_stars")
        self.init(/**id: id,*/ user_name: name, level: level, total_round: total_round, top_points: top_point, stars: stars)
    }
}
