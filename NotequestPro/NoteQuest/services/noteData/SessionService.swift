//
//  SessionService.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 05.03.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation
import UIKit

class SessionService:NSObject {
    private var view:SessionVC
    private var data = SessionData()
    private var timer:SessionTime?
    
    private var freqService:FrequencyService?
  
    private var keyboard:KeyBoardView?
    private var inited = false
    
    init(_ view:SessionVC){
        self.view = view
    }
    
    func startSession(){
        if(!inited){
            self.listenNavigation()
            inited = true
            clearHint()
            data.initThis(level: UserSets.level(), mode: UserSets.mode() )
            self.prepareMode(UserSets.mode())
        }
        processStart()
    }
    
    private var mode:QuestMode?
    private func prepareMode(_ mode:QuestMode){
        self.mode = mode
        view.presentNode(mode: mode)
        if(UserSets.mode() == QuestMode.Virtual){
            var h = 180
            if(isIpad()){
                h = 250
            }
            keyboard = attachKeyboard(view.keyboardScroll, height: CGFloat(h), width:UIScreen.main.bounds.width)
        }
    }
    
    private func processStart(){
        data.reset()
        timer = SessionTime(fullTime:LevelHelps.levelTime(levelID: UserSets.level())){
            self.updateTime()
        }
        timer?.start()
        updateTime()
        if(mode == .Real){
            freqService?.stop()
            freqService = FrequencyService(self)
            freqService?.start()
        }
        
        if let note = self.data.currentCell(){
            self.view.presentImage(note)
        }
    }
    
    func disappear(){
        timer?.stop()
        freqService?.stop()
        freqService = nil
        keyboard?.dismiss()
        removeListenNavigation()
    }
    
    private func processFrequency(freq:Double){
        if(data.isEmpty()){
            return
        }
        guard timer?.played() == true else { return}
        guard let note = data.currentCell() else { return }
        guard let frequencyToPlay = note.freq() else { return }
        
        if(checkRealNote(checked: freq, needed: Double(frequencyToPlay))){
            processNextNote(true)
        } else {
            if(UserSets.level()>2){
                showTitle("Try Again")
            } else {
                if( freq > Double(frequencyToPlay)){
                    showTitle("Too high")
                } else{
                    showTitle("Too low")
                }
            }
        }
    }
    
    func skip(){
         if (!inited || data.isEmpty()) {return}
        processNextNote(false)
    }
    
    private func processNextNote(_ prevSuccess:Bool){
        clearHint()
        if UserSets.level() > 2, let next = data.currentCell()?.nextFreq(prevSuccess) {
            print("note was found, next \(next)")
        } else {
            showTitle("Perfect")
            if let newCell = data.nextCell(prevSuccess){
                self.view.presentImage(newCell)
            } else {
                self.finishLevel()
            }
        }
    }
    
    private var hintDelay:Timer?
    func presentHint(){
        if (!inited  || data.isEmpty()) {return}
        
        switch UserSets.mode() {
        case QuestMode.Virtual:
            keyboard?.hintFrequences(data.currentCell()?.frequences.map{ $0.freq()} )
            break
            
        case QuestMode.Real:
            view.showHint( data.currentCell()?.notesStr() ?? "")
            break
            
        }
        hintDelay?.invalidate()
        hintDelay = Timer.scheduledTimer(withTimeInterval: 1, repeats: false){_ in
            self.clearHint()
        }
    }
    
    @objc func clearHint(){
        DispatchQueue.main.async {
            switch UserSets.mode() {
            case QuestMode.Virtual:
                self.keyboard?.clearHint()
            case QuestMode.Real:
                self.view.clearHint()
            }
        }
    }
    
    private func showTitle(_ str:String){
   //     if(AppSets.DEVELOPER_MODE){
   //         view.showHint(str)
   //     }
    }
    
    @objc func updateTime() {
        if let startTime = timer {
            if startTime.secondsFromStart() >= startTime.fullTime {
                self.finishLevel()
            } else {
               self.view.presentTime( startTime.strFromStart() )
            }
        }
    }
    
    private func finishLevel(){
        freqService?.stop()
        freqService = nil
        timer?.stop()
        FinishSessionData.lastFinishedData = FinishSessionData(time:Double(timer?.timeFromStart() ?? 0), data:data)
        self.view.presentTime("")
        self.view.popup(Storyboards.FinishLevelVC, storyboard: Storyboards.alerts)
    }
}

extension SessionService:IFrequenceListener{
    func listenFrequency(freq: Double) {
       processFrequency(freq: freq)
    }
}

extension SessionService:KeyboardViewDelegate{
    func keysPressed(frequency: Double) {
         processFrequency(freq: frequency)
    }
}

extension SessionService:UINavigationListener{
    func onScreenClosed(_ screen: String) {
        if(screen == Storyboards.FinishLevelVC){
            guard let state = FinishSessionData.lastFinishedData?.state else {
                startSession()
                return
            }
            
            switch( state){
            case .NONE:
                return;
            case FinishSessionResult.CHOOSE_LEVEL:
                let pos = (view.navigationController?.viewControllers.count ?? 1) - 1
                view.screen(Storyboards.SelectLevelVC)
                view.navigationController?.viewControllers.remove(at: pos)
                break
            case FinishSessionResult.CANCEL:
                view.navigationController?.popViewController(animated: true)
                break
            default:
                startSession()
                break;
            }
            FinishSessionData.lastFinishedData = nil
        }
    }
}
