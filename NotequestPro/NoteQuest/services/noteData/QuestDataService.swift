//
//  QuestDataService.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 05.03.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation

enum QuestMode{
    case Real
    case Virtual
}

func modeName(_ type:QuestMode)->String{
    switch type {
    case .Real:
        return "Real"
    case .Virtual:
        return "Virtual"
    }
}
func modeByName(_ name:String)->QuestMode{
    if(name == "Real"){
        return QuestMode.Real
    } else {
        return QuestMode.Virtual
    }
}

func loadNotes(level:Int, type:QuestMode, onSuccess: @escaping (Array<Any>?)->Void){
    if let filepath = Bundle.main.path(forResource: "level_\(level)", ofType: nil) {
        do {
            let contents = try String(contentsOfFile: filepath)
            let data = contents.data(using: .utf8)!
            
            guard let dict =  try? (JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as? NSDictionary) else {
                onSuccess(nil)
                return
            }
            
            guard dict?["status"] as? Int  == 1 else {
                onSuccess(nil)
                return
            }
            
            guard let parsed = dict?["freq"] as? Array<Any> else {
                onSuccess(nil)
                return
            }
            
            onSuccess(parsed)
        } catch {
            onSuccess(nil)
        }
    } else {
        onSuccess(nil)
    }
}
