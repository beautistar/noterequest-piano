//
//  SessionDataHelpers.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 19.02.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation

class SessionData{
    private var objects = [NoteCell]()
    private var currIdx = 0
    private var successed = 0
    private var total = 0
    private var level = 0
    private var successScreens = 0
    private var mode = QuestMode.Real
    func initThis(level:Int, mode:QuestMode){
        self.level = level
        self.mode = mode
       reset()
    }
    
    func reset(){
        currIdx = 0
        successed = 0
        total = 0
        successScreens = 0
        
        let notes = LevelGenerator.generate(level)
        self.objects = notes
        for note in notes{
            self.total += note.freqSize()
            note.reset()
        }
    }
    
    func currentCell()->NoteCell?{
        if(objects.count>currIdx){
            return objects[currIdx]
        } else {
            return nil
        }
    }
    
    func isEmpty()->Bool{
        return objects.count == 0
    }
    
    func nextCell(_ isSuccess:Bool)->NoteCell?{
        if let cell = currentCell(), isSuccess {
            successed = successed + cell.freqSize()
            if(!cell.containsSkipped()){
                successScreens += 1
            }
        }
        
        currIdx = currIdx + 1;
        if let result = currentCell(){
            result.reset()
            return result
        } else {
            return nil
        }
    }
    
    func totalCount()->Int { return total }
    func succCount()->Int { return successed }
    func stepsCompleted()->Int { return currIdx }
    func succScreens()->Int{ return successScreens }
    
    func scoreInPercents()->Int {
        if(total == 0){
            return 0
        }else {
            return Int(Double(successed)/Double(total) * 100)
        }
    }
    
    func scoreStr()->String{
        return "\(successed)/\(total)"
    }
}

