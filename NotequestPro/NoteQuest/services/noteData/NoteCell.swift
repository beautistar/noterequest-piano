//
//  NoteCell.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 19.02.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation

class NoteCell:NSObject{
    let frequences: Array<NoteData>
    
    func notes()->[String]{
        return frequences.map{$0.title() }
    }
    
    func notesStr()->String{
        let notes = self.notes()
        var result = " "
        for note in notes{
            result = result + note + " "
        }
        result = result.subStr(to: result.count-1).removeDigits()
        return result
    }
    
    func copy()->NoteCell{
        return NoteCell( frequences)
    }
    
    private var _skipped = false
    private var currentNoteIdx = 0
    func containsSkipped()->Bool{
        return _skipped
    }
    
    init(_ parts:[NoteData]){
        frequences = parts
    }
    
    func nextFreq(_ isSuccess:Bool)->Double?{
        if(!isSuccess){
            _skipped = true
        }
        currentNoteIdx = currentNoteIdx + 1
        return freq()
    }
    
    func freq() -> Double?{
        if ( currentNoteIdx < frequences.count ){
            return frequences[currentNoteIdx].freq()
        }else{
            return nil
        }
    }
    
    func reset(){
        currentNoteIdx = 0
        _skipped = false
    }
    
    func freqSize()->Int{
        return frequences.count
    }
}
