//
//  LevelHelps.swift
//  NoteQuest
//
//  Created by Aleksandrov Denis on 13.03.2018.
//  Copyright © 2018 Mentalstack. All rights reserved.
//

import Foundation
import UIKit

struct LevelHelp {
    let title:String
    let desc:String
    let image:UIImage?
}

struct LevelListDescription{
    let num:Int
    let title:String
    let desc:String
}

class LevelHelps{
    
    static let Level_sets = [
        LevelListDescription(num: 1001, title: "3 Landmark Notes", desc: "Middle C, Bass F, Treble G"),
        LevelListDescription(num: 1002, title: "The 3 C’s", desc: "Bass C, Middle C, Treble C"),
        LevelListDescription(num: 1003, title: "The 5 C’s", desc: "Low C to High C (ledger line C’s)"),
        LevelListDescription(num: 1004, title: "Landmark Note Mix", desc: "All Landmark Notes"),
        LevelListDescription(num:1, title: "Starter", desc: "Middle C + surrounding notes"),
        LevelListDescription(num:2, title: "Rising Star", desc: "Bass C to Treble C, 2 octave range"),
        LevelListDescription(num:3, title: "Two Stepper", desc: "+ Intervals, 2 octave range"),
        LevelListDescription(num:4, title: "Navigator", desc: "+ ♭, #, ♮, 3 octave range"),
        LevelListDescription(num:5, title: "Master", desc: "+ Ledger lines, 4 octave range"),
        LevelListDescription(num:6, title: "NEW", desc: "Interval Selector")
    ]
    
   static let levels = [
        LevelHelp(title: "Level 1", desc: "Bass F up to Treble G, 9 pitches only", image: #imageLiteral(resourceName: "level1")),
        LevelHelp(title: "Level 2", desc: "Bass C up to Treble C, 2 octave range", image: #imageLiteral(resourceName: "level2")),
        LevelHelp(title: "Level 3", desc: "Includes 2-note intervals, 2 octave range, white keys", image: #imageLiteral(resourceName: "level3")),
        LevelHelp(title: "Level 4", desc: "Includes black keys (# ,♭, ♮) 3 octave range, all keys ", image: #imageLiteral(resourceName: "level4")),
        LevelHelp(title: "Level 5", desc: "Includes ledger line notes. All of the previous concepts: 4 octave range, all keys & intervals", image: #imageLiteral(resourceName: "level5")),
    ]
    
    static func getLevelHelp(_ level:Int)->LevelHelp{
        if(level<=levels.count){
            return levels[level-1]
        } else {
            return levels[0]
        }
    }
}
